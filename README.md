# Complete JavaScript course with projects (Bangla)
course duration: almost 50 hours

Topics covered in premium JavaScript course:

1. Introduction
    1. Thank you for joining
    2. Pre-Requisite to complete the course
    3. What to expect from the course?
    4. How to get most out of the course
    5. where to get help when you stuck
    6. Some Inspiration and Advancement of your carrier
    7. What we are going to cover

2. Let’s Drive into JavaScript
    * Introduction to JavaScript
    * What & why & how JavaScript?
    * Code editor
    * Prepare to write code
    * Type of First JavaScript code
    * Where to put JavaScript code
    * Getting familiar with Node

3. JavaScript fundamentals
    - Let’s talk to machine - DataType
    - String
    - Number
    - Concept of variables
    - Let & const
    - Data Type Exercise
    - Dynamic typing
    - Complex data type- Array
    - Array in Practice
    - Array-Exercise
    - Complex data type- Object
    - Object in practice
    - Exercise-object
    - Exercise Solve- Movie DB
    - Introduction to Function
    - Function in practice
    - JavaScript statement
    - Template string
    - Return in practice
    - Return keyword
    - Exercise function
    - Null vs undefined

4. OPERATORS
    - Introduction to operator
    - Arithmetic operator
    - Working with Quokka js
    - Assignment operator
    - Operands++ vs ++operands
    - Comparison operator
    - Equality operator
    - == vs ===
    - Truthy and falsy value
    - Checking non-Boolean value
    - Operator precedence
    - Interesting case
    - Exercise (total 4 exercises)

5. Control Flow
    - Introduction to logic & condition
    - If else statement
    - Combining multiple condition
    - Ternary operator
    - Conditioning with Logical and or operator
    - If & Else if
    - Switch case statement
    - Exercise (Total 2 exercises)

6. Loop
    - Introduction to while loop
    - While loop in practice
    - Introduction to for loop
    - For loop in practice
    - Looping with array
    - Break && continue
    - For of loop
    - For in loop
    - Dynamically access properties
    - Exercise -loop (total 6 exercises)

7. More Objects
    - Object review
    - Method
    - Factory Function
    - Constructor function
    - “This” in JavaScript
    - Call, bind, apply
    - Binding “this” in JavaScript
    - Function expression and this binding
    - Copy by value/copy by reference
    - Pass by value /pass by reference
    - Copying object
    - Functions as a object
    - Math Object
    - Date Object
    - Exercise (Total 5 exercises)

8. More Function
    - Function Review
    - Arguments
    - Rest operator
    - Default parameter
    - Evolution of function
    - Arrow function and “this”
    - First class function
    - Difference between function statement and Expression
    - Hoisting
    - Scope
    - Var vs Let vs const
    - Rest and spread operator
    - Exercise (total 2 exercises)

9. More ARRAYS
    - Array Review
    - Adding Elements into array
    - Removing elements from array
    - Finding Elements(primitive)
    - Finding Element (Reference)
    - Find vs filter
    - Combining array
    - Split and join
    - Copy array
    - Emptying array
    - Iterating array
    - Sorting array
    - Every and some helper
    - Map method and chaining
    - Exercise (Total 5 Exercises)

10. DOM
    1. What is DOM
    1. DOM in practice
    1. Window object
    1. Selecting element
    1. HTML collection vs Node List
    1. Manipulating element
    1. Traversing DOM
    1. Interacting and child Nodes
    1. Creating Element
    1. Replacing and removing element
    1. Event Introduction
    1. Event in practice
    1. It’s your time to practice
    1. Event bubbling and delegation
    1. Event delegation in practice
    1. Exercise (Total 4 Exercises)

###11. Project-1 (player vs player -small introductory project with all previous concept)
Introduction to the project
HTML boilerplate
Selection and data declaration
Working with player score
Tracking winner state
Code refactoring
Dynamic winning score
Code refactoring (IIFE)
Homework Idea (Develop your own)

###12. Project-2 (product catalog - All things come together , written in procedural programming way)
Introduction to the project
Header and filter Design
Complete the Design process
Selection and creating store
Populating item
Adding item validation
Adding item to data store
Generating unique id for each item,
Deleting item
Searching item
Formatting message
Organization
Introduction to local Storage
Local Storage solves your problem
Setting Item
Getting Item
Saving and getting object from local Storage
Applying local Storage in our project
Correcting error
Homework (Develop your tweet tweet project)
HTML boilerplate
Selection and data declaration
Working with player score
Tracking winner state
Code refactoring
Dynamic winning score
Code refactoring (IIFE)
Homework Idea (Develop your own)

###13. Object oriented JavaScript
Return Multiple object from function
Constructor function
Constructor function declared by JavaScript
Object constructor
Inheritance
ES6 class

###14.Project -3 (profile Builder App- written in Object oriented JavaScript both in ES5 and ES6 )
Building HTML structure
Adding item to UI
Validation and Clear Field after submission
Delete profile
Showing message
Converting to ES6
Adding item to local Storage
Getting and Displaying item from local Storage
Deleting item from local Storage
Homework (writing product catalog app using OOJ)

###15. Asynchronous JavaScript
Synchronous programming
Introduction to asynchronous programming
Dealing asynchronous code with callback
Simple practical example of Callback and callback hell
Introduction to Promise
Converting existing example to use promise
Promise resolve, reject, all, race
Using Async await
Try catch with async await

###16.AJAX, JSON and API
How web works?
Type of Request and REST API Introduction
What is API (Application programming interface)
What is AJAX?
Where AJAX seats in
Working with AJAX
AJAX shortcut method
What is JSON
Using JSON data
Using fetch
Sample profile using JSON
Working with Jokes API
Working
With JSON placeholder API
JSON placeholder in practice
Building Http Library
Homework (Develop a project with ICNDB Jokes API)

###17. project-4(weather App using 3rd party API -written in Object oriented JavaScript using promise , Async await)
Introduction to the project
HTML structure
Exploring API
Getting Idea
Serving data to UI
Generating and serving icon
Generating dynamic data from form
Working with local Storage
Handling message
Separate functionality by file
Homework (Develop GitHub profile finder by using GitHub API)

###18.Webpack, babel and ES6 module
Some problem we are facing and solution!!
More about babel and webpack
Installing babel and webpack
Configuring webpack
Configuring babel and webpack continued
Es6 module
Practical use of ES6 module
Homework (use ES6 module to convert profile builder APP)

###19.project-5 (Contact APP using REST API- Developed by using  REST API)
Project Showcase
Installing JSON server
Configuring and running webpack
REST API principal
Writing HTML markup
Getting contact after DOM Load
Working with UI
Working with Form data
Submitting Form data
Deleting contact
Little organization
Populating form
Working with edit state
Changing state after updating contact
Showing message
Showing Meaningful message in Application

###20.Introduction to DESIGN pattern
Scope revisited
Closure Explained
Introduction to Design pattern
Modular Design pattern

###21.Projects-6 (Task Manger- Developed by using modular Design pattern)
Overview of the project
HTML Mark Up
Continue HTML Mark Up
Different module and responsibilities
How to work with module internally
Populating task in UI
Working UI state
Working with init and Event listener
Working with task submission
Updating data dynamically
Adding new task
Showing container conditionally
Working with completed task functionality
Adding CSS class Dynamically
Working with edit state
Updating task
Dealing with back Button
Updated complete task count
Adding task to local Storage
Getting task from local Storage
Updating task in local Storage
Deleting task from local Storage
Showing alert message in UI
Solving existing issues
Home work (Develop Task app using ES6 module)

###22.What to learn next?
- Now You are a Hero of JavaScript
- Targeting front-end development
- Targeting mobile application Development
- Targeting server-side application Development
- Targeting Desktop and continue more advanced application Development

###23.What are the Next steps?
- React, Angular, Vue(frontend)
- Node (Backend)
- Electron (Desktop)
- NativeScript, React Native(Native Mobile application)

###24. GoodBye folks
- Thank you for being awesome and patient