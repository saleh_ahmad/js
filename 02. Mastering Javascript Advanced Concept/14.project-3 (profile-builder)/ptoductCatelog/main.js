const filterInput = document.querySelector('#filter'),
    nameInput = document.querySelector('#product-name'),
    priceInput = document.querySelector('#product-price'),
    addBtn = document.querySelector('#add-product'),
    productList = document.querySelector('.collection'),
    msg = document.querySelector('#msg'),
    formSelector = document.querySelector('form'),
    collectionItems = document.querySelectorAll('.collection .collection-item');

class Store {
    static getData() {
        return (localStorage.getItem('productItems') === null)
            ? []
            : JSON.parse(localStorage.getItem('productItems'));
    }

    static displayItems() {
        const items = Store.getData();
        items.forEach(item => {
            const ui = new UI();
            ui.addData(item);
        });
    }

    static setData(item) {
        const items = (localStorage.getItem('productItems') === null)
            ? []
            : JSON.parse(localStorage.getItem('productItems'));

        items.push(item);
        localStorage.setItem('productItems', JSON.stringify(items));
    }

    static deleteItem(id) {
        const items = JSON.parse(localStorage.getItem('productItems'));

        let result = items.filter((product) => {
            return product.id !== id;
        });

        localStorage.setItem('productItems', JSON.stringify(result));

        if (result.length === 0) {
            location.reload();
        }
    }
}

class UI {
    addData({id, name, price}) {
        let li;

        li = document.createElement('li');
        li.classList.add('list-group-item', 'collection-item');
        li.setAttribute('data-id', id);
        li.innerHTML = `<strong>${name}</strong> - $<span class="price">${price}</span> <i class="fas fa-trash-alt float-right dlt-btn"></i>`;

        productList.appendChild(li);
    }

    static showAlertMsg(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.textContent = message;

        formSelector.insertAdjacentElement('beforebegin', div);

        setTimeout(() => {
            document.querySelector('.alert').remove();
        }, 2000);
    }

    static showMessage(message = '') {
        msg.innerHTML = message;
    }

    getId() {
        const items = JSON.parse(localStorage.getItem('productItems'));

        return (items.length === 0)
            ? 0
            : items[items.length - 1].id + 1;
    }
}

//Load All Event Listener
class EventListeners {
    static addItem = e => {
        e.preventDefault();

        const name = nameInput.value,
            price = priceInput.value;

        const ui = new UI();
        const id = ui.getId();

        //Validation
        if (name === '' || price === '') {
            UI.showAlertMsg('Please fill up necessary information.', 'danger');
            return;
        }

        if (!(!isNaN(parseFloat(price)) && isFinite(price))) {
            UI.showAlertMsg('Please insert a valid price.', 'danger');
            return;
        }

        const data = {
            id,
            name,
            price
        };

        //Save to Local Storage
        Store.setData(data);

        //Show data in the UI
        ui.addData(data);

        nameInput.value = priceInput.value = '';
    };

    static deleteItem= e => {
        if (e.target.classList.contains('dlt-btn')) {
            //Removing target from UI
            const target = e.target.parentElement;
            e.target.parentElement.parentElement.removeChild(target);

            /*
             * Removing Item from store
             * return result array
             */
            const id = parseInt(target.dataset.id);

            productData = productData.filter((product) => {
                return product.id !== id;
            });

            Store.deleteItem(id);
            UI.showAlertMsg('One item has been successfully deleted.', 'success');
        }
    };

    //Filter Item
    static filterItem = e => {
        const text = e.target.value.toLowerCase();
        let itemFound = false;

        document.querySelectorAll('.collection .collection-item')
            .forEach(item => {
                const productName = item.firstElementChild.textContent.toLowerCase();
                const productPrice = item.children[1].textContent.toLowerCase();

                if ((productName.indexOf(text) === -1) && (productPrice.indexOf(text) === -1)) {
                    item.style.display = 'none';
                } else {
                    item.style.display = 'block';
                    itemFound = true;
                }
            });

        itemFound ? UI.showMessage() : UI.showMessage('No Item Found.');
    };

    static load() {
        //Load Data
        window.addEventListener('DOMContentLoaded', Store.displayItems);

        //Add Item
        addBtn.addEventListener('click', EventListeners.addItem);

        //Delete Item
        productList.addEventListener('click', EventListeners.deleteItem);

        //Filter Item
        filterInput.addEventListener('keyup', EventListeners.filterItem);
    }
}

EventListeners.load();
