const profileList = document.querySelector('#profile-list'),
    nameSelector = document.querySelector('#name'),
    emailSelector = document.querySelector('#email'),
    professionSelector = document.querySelector('#profession'),
    formSelector = document.querySelector('form');

function Profile(name, email, profession) {
    this.name = name;
    this.email = email;
    this.profession = profession;
}

function UI() {}

//Adding Profile
UI.prototype.addProfileToLIst = function ({name, email, profession}) {
    const tr = document.createElement('tr');
    tr.innerHTML = `<td>${name}</td>
            <td>${email}</td>
            <td>${profession}</td>
            <td><i class="fa fa-trash delete-btn"></i></td>`;

    profileList.appendChild(tr);
}

//Clear Input Fields
UI.prototype.clearField = function() {
    nameSelector.value = '';
    emailSelector.value = '';
    professionSelector.value = '';
}

//Removing Profile
UI.prototype.deleteProfile = function(target) {
    if (target.classList.contains('delete-btn')) {
        target.parentElement.parentElement.remove();
    }
}

//Show Alert Message
UI.prototype.showAlertMsg = function(message, className) {
    const div = document.createElement('div');
    div.className = `alert alert-${className}`;
    div.textContent = message;

    formSelector.insertAdjacentElement('beforebegin', div);

    setTimeout(() => {
        document.querySelector('.alert').remove();
    }, 2000);
}

document.querySelector('form').addEventListener('submit', e => {
    e.preventDefault();

    const name = nameSelector.value;
    const email = emailSelector.value;
    const profession = professionSelector.value;

    //Instantiate UI Object
    const ui = new UI();

    if (name === '' || email === '' || profession === '') {
        ui.showAlertMsg('Please fill-up the necessary information.', 'danger');
        return;
    }

    //Instantiate Profile Object
    const profile = new Profile(name, email, profession);

    ui.addProfileToLIst(profile);
    ui.clearField();
    ui.showAlertMsg('A profile has been added.', 'success');
});

//Event Delegation
profileList.addEventListener('click', e => {
    //Instantiate UI Object
    const ui = new UI();
    ui.deleteProfile(e.target);
    ui.showAlertMsg('A profile has been removed.', 'success');
});