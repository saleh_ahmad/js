const profileList = document.querySelector('#profile-list'),
    nameSelector = document.querySelector('#name'),
    emailSelector = document.querySelector('#email'),
    professionSelector = document.querySelector('#profession'),
    formSelector = document.querySelector('form');

class Profile {
    constructor(id, name, email, profession) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.profession = profession;
    }
}

class UI {
    addProfileToLIst({id, name, email, profession}) {
        const tr = document.createElement('tr');
        tr.setAttribute('data-id', id);
        tr.innerHTML = `<td>${name}</td>
            <td>${email}</td>
            <td>${profession}</td>
            <td><i class="fa fa-trash delete-btn"></i></td>`;

        profileList.appendChild(tr);
    }

    clearField() {
        nameSelector.value = '';
        emailSelector.value = '';
        professionSelector.value = '';
    }

    deleteProfile(target) {
        if (target.classList.contains('delete-btn')) {
            const id = target.parentElement.parentElement.dataset.id;
            Store.deleteProfile(id);
            target.parentElement.parentElement.remove();
        }
    }

    showAlertMsg(message, className) {
        const div = document.createElement('div');
        div.className = `alert alert-${className}`;
        div.textContent = message;

        formSelector.insertAdjacentElement('beforebegin', div);

        setTimeout(() => {
            document.querySelector('.alert').remove();
        }, 2000);
    }

    getId() {
        return document.querySelectorAll('tr').length;
    }
}

class Store {
    static addToStorage(profile) {
        let profiles;

        if (localStorage.getItem('profiles') === null) {
            profiles = [];
        } else {
            profiles = JSON.parse(localStorage.getItem('profiles'));
        }

        profiles.push(profile);
        localStorage.setItem('profiles', JSON.stringify(profiles));
    }

    static getProfiles() {
        return (localStorage.getItem('profiles') === null)
            ? []
            : JSON.parse(localStorage.getItem('profiles'));
    }

    static displayProfiles() {
        const profiles = Store.getProfiles();
        profiles.forEach(profile => {
            const ui = new UI();
            ui.addProfileToLIst(profile);
        });
    }

    static deleteProfile(id) {
        const profiles = Store.getProfiles();
        profiles.forEach((profile, index) => {
            if (profile.id === Number(id)) {
                profiles.splice(index, 1);
            }
        });
        localStorage.setItem('profiles', JSON.stringify(profiles));
    }
}

class EventListeners {
    static addProfile = e => {
        e.preventDefault();

        const name = nameSelector.value;
        const email = emailSelector.value;
        const profession = professionSelector.value;

        //Instantiate UI Object
        const ui = new UI();
        const id = ui.getId();

        if (name === '' || email === '' || profession === '') {
            ui.showAlertMsg('Please fill-up the necessary information.', 'danger');
            return;
        }

        //Instantiate Profile Object
        const profile = new Profile(id, name, email, profession);

        //Add to UI
        ui.addProfileToLIst(profile);

        //Add to local storage
        Store.addToStorage(profile);

        ui.clearField();
        ui.showAlertMsg('A profile has been added.', 'success');
    };

    static deleteProfile = e => {
        //Instantiate UI Object
        const ui = new UI();
        ui.deleteProfile(e.target);
        ui.showAlertMsg('A profile has been removed.', 'success');
    };

    static load() {
        window.addEventListener('DOMContentLoaded', Store.displayProfiles);
        formSelector.addEventListener('submit', EventListeners.addProfile);
        profileList.addEventListener('click', EventListeners.deleteProfile);
    }
}

EventListeners.load();