//common js modular patter - used by Node js internal
//Es6 modular pattern
import addExtra, { add } from './add';

// import { firstName, lastName } from './sample';
import * as fullName from './sample';
import multiply from './multiply';
console.log(fullName.default, fullName.firstName, fullName.lastName);
console.log(multiply(2, 3));
console.log(add(1, 2));
console.log(addExtra(1, 2, 3));
