function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.fullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }
}

const person = new Person('Saleh', 'Ahmad');
console.log(person.fullName());

const person2 = new Person('Shakib', 'Hasan');
console.log(person2.fullName());