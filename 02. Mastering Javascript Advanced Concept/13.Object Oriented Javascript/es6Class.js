class Person {
    //Constructor
    constructor(firstName, lastName) {
        this.firstName = firstName;
        this.lastName  = lastName;
    }

    fullName() {
        return `${this.firstName} ${this.lastName}`;
    }
}

//Inheritance
class Student extends Person {
    constructor(firstName, lastName, enrolledCourse) {
        super(firstName, lastName);
        this.enrolledCourse = enrolledCourse;
    }

    studentWithCourse() {
        return `${super.fullName()} enrolled in ${this.enrolledCourse}.`;
    }

    //Overriding
    fullName() {
        return super.fullName() + ' [Override in child class]';
    }

    //Static Method
    static Income() {
        return 100;
    }
}

const student = new Student('Saleh', 'Ahmad', 'JavaScript');
console.log(student);
console.log(student.fullName());
console.log(student.studentWithCourse());
console.log(Student.Income());