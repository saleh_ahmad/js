//String
//We will use this way
let str = ' Hello ';
str = str.toLowerCase();
console.log(str);
console.log(str.length);

str = str.toUpperCase();
console.log(str);
console.log(str.length);

str = str.trimLeft();
console.log(str);
console.log(str.length);

str = str.trim();
console.log(str);
console.log(str.length);

const strC = new String('Hello');
console.log(strC);

//Number
const num = 13;
console.log(num);

const numC = new Number(13);
console.log(numC);

//Boolean
const bool = true;
console.log(bool);

const boolC = new Boolean(true);
console.log(boolC);

//array
const arr = [1, 2, 3];
console.log(arr);

const arrC = new Array([1, 2, 3]);
console.log(arrC);

//Object
const obj = {
    fName : 'Saleh',
    lName : 'Ahmad'
};
console.log(obj);

const objC = new Object({
    fName : 'Saleh',
    lName : 'Ahmad'
});
console.log(objC);