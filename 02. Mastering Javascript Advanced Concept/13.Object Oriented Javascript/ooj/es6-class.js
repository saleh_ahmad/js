//Syntactic sugar - Different way of writing code but behind the scene it works as the way before
class Person {
  constructor(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }
  //Declaring method inside class: Behind the scene works as a prototype
  fullName() {
    return this.firstName + ' ' + this.lastName + 'from proto';
  }
}

//Inheriting Person class
class Student extends Person {
  constructor(firstName, lastName, enrolledCourse) {
    //calling parent class constructor by super with necessary arguments
    super(firstName, lastName);
    this.enrolledCourse = enrolledCourse;
  }
  studentWithCourse() {
    console.log(
      `${this.firstName} ${this.lastName} enrolled in ${this.enrolledCourse}`
    );
  }
  fullName() {
    //You can access parent method by super
    return super.fullName() + 'extra edit';
  }
  //static method is private to the class you can't access by instantiating, only can access by Class.method()
  static Income() {
    console.log('Secret amount $300');
  }
}
//Instantiating Student class
const student = new Student('samim', 'Hasan', 'Javascript');
//calling static method
console.log(Student.Income());
console.log(student.studentWithCourse());
console.log(student.fullName());
console.log(student);
// const person = new Person('samim', 'Hasan');
// console.log(person);
// console.log(person.fullName());
