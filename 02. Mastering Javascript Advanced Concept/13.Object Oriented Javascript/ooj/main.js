//Constructor function Person, Naming is important always capitalize your first word
function Person(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;
}
//Benefit of using prototype- Only a single copy created
Person.prototype.fullName = function() {
  console.log(this.firstName + ' ' + this.lastName + 'from proto');
};

//Inherit all capabilities of parent constructor function(Inheritance)
function Student(firstName, lastName, enrolledCourse) {
  //call the Person constructor with required arguments
  Person.call(this, firstName, lastName);

  this.enrolledCourse = enrolledCourse;
}

Student.prototype.studentWithCourse = function() {
  console.log(
    `${this.firstName} ${this.lastName} enrolled in ${this.enrolledCourse}`
  );
};

//Weird line is used to inherit parent prototype method
Student.prototype.__proto__ = Person.prototype;
//Instantiate Student constructor resulting a object
const student = new Student('samim', 'Hasan', 'Javascript');
console.log(student);
console.log(student.studentWithCourse());
console.log(student.fullName());

//calling constructor Person and result a object which is assigned to person1, person2

// const person1 = new Person('samim', 'Hasan');
// const person2 = new Person('khalil', 'Rahman');
//constructor function prototype indicate same thing object proto create by calling same constructor function
// console.log(Person.prototype === person1.__proto__);
// console.log(person1);
// console.log(person1.fullName());

//order of prototype chain
//========================================================
// Array: myArray --> Array.prototype --> Object.prototype --> null
// Function: myFunc --> Function.prototype --> Object.prototype --> null
// String: myString --> String.prototype --> Object.prototype --> null
// Number: myNumber --> Number.prototype --> Object.prototype --> null
// Boolean: myBoolean --> Boolean.prototype --> Object.prototype --> null

// const str = ' Hello ';
// str.toLocaleLowerCase();
// const strWithTrim = str.trim();
// console.log(strWithTrim.length);

//Creating string with constructor
// const strC = new String('Hello');
// console.log(strC);

// let number = 20;

//Creating number with constructor

// number = new Number(18);
// let boolean = true;

//Creating boolean with constructor
// boolean = new Boolean(true);

// let arr = [1, 2, 3];

//Creating array with constructor
// arr = new Array(1, 2, 3);
// let obj = { firstName: 'samim' };

//Creating object with constructor
// obj = new Object({
//   firstName: 'samim'
// });
// console.log(obj);

// function Person(firstName, lastName) {
//   this.firstName = firstName;
//   this.lastName = lastName;
//   this.fullName = function() {
//     console.log(this.firstName + ' ' + this.lastName);
//   };
// }

//calling constructor Person and result a object which is assigned to person1, person2

// const person1 = new Person('samim', 'Hasan');
// const person2 = new Person('khalil', 'Rahman');
// console.log(person1.firstName);
// console.log(person2.firstName);
// console.log(person1.fullName());
// console.log(person2.fullName());

//Better way to produce variable object

// function person(firstName, lastName) {
//   return {
//     firstName: firstName,
//     lastName: lastName,
//     fullName: function() {
//       console.log(this.firstName + this.lastName);
//     }
//   };
// }

// console.log(person('samim', 'Hasan'));
// console.log(person('Khalil', 'Rahman'));
// const person2 = person('Khalil', 'Rahman');
// console.log(person2.fullName());

//simple way of creating object
// const person1 = {
//   firstName: 'samim',
//   lastName: 'Hasan',
//   fullName: function() {
//     console.log(this.firstName + this.lastName);
//   }
// };
// const person2 = {
//   firstName: 'Khalil',
//   lastName: 'Rahman',
//   fullName: function() {
//     console.log(this.firstName + this.lastName);
//   }
// };

// console.log(person1);
// console.log(person1.fullName());
// console.log(person2);
// console.log(person2.fullName());
