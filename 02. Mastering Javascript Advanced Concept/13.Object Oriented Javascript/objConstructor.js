function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.fullName = function() {
        return `${this.firstName} ${this.lastName}`;
    }
}

//This is the best way to add function
Person.prototype.fullNameProto = function() {
    return `FullName: ${this.firstName} ${this.lastName} from proto`;
}

//order of prototype chain
//========================================================
// Array: myArray --> Array.prototype --> Object.prototype --> null
// Function: myFunc --> Function.prototype --> Object.prototype --> null
// String: myString --> String.prototype --> Object.prototype --> null
// Number: myNumber --> Number.prototype --> Object.prototype --> null
// Boolean: myBoolean --> Boolean.prototype --> Object.prototype --> null

const person = new Person('Saleh', 'Ahmad');
console.log(person.fullName());
console.log(person.fullNameProto());
console.log(Person.prototype === person.__proto__);

console.log(person);

const person2 = new Person('Shakib', 'Hasan');
console.log(person2.fullName());