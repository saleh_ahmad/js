function person(firstName, lastName) {
    return {
        firstName,
        lastName,
        fullName : () => {
            return `${firstName} ${lastName}`;
        }
    }
}

console.log(person('Saleh', 'Ahmad').fullName());