function Person(firstName, lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
}

Person.prototype.fullName = function() {
    return `FullName: ${this.firstName} ${this.lastName} from proto`;
}

function Student(firstName, lastName, enrolledCourse) {
    Person.call(this, firstName, lastName);
    this.enrolledCourse = enrolledCourse;
}

Student.prototype.studentWithCourse = function() {
    return `${this.firstName} ${this.lastName} enrolled in ${this.enrolledCourse}.`;
}

const student = new Student('Saleh', 'Ahmad', 'Javascript');
console.log(student.studentWithCourse());
//Will not find
//console.log(student.fullName());

Student.prototype.__proto__ = Person.prototype;
console.log(student.fullName());

console.log(student);