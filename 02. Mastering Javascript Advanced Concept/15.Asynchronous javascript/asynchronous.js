//Javascript is asynchronous in nature
console.log(1);
setTimeout(() => {
    console.log('calling after 2 second: ', 2);
}, 2000);
console.log(3);