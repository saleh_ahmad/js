class Ui {
  constructor() {
    //selectors
    this.containerForm = document.querySelector('.containerForm');
    this.contactInput = document.getElementById('contacts');
    this.firstNameInput = document.getElementById('firstName');
    this.lastNameInput = document.getElementById('lastName');
    this.emailInput = document.getElementById('email');
    this.phoneInput = document.getElementById('phone');
    this.contactSubmit = document.getElementById('contact-submit');
    this.contactForm = document.querySelector('.contact-form');
    this.FormEnd = document.querySelector('.form-end');
    this.idInput = document.getElementById('id');
  }
  paint(contacts) {
    let output = '';
    contacts.forEach(contact => {
      //Destructuring contact
      const { firstName, lastName, email, phone } = contact;
      output += `
      <div class="card">
          <div class="card-body">
            <h5 class="card-title">${firstName} ${lastName}</h5>
            <p class="card-text">${email}</p>
            <p class="card-text">${phone}</p>
            <a href="#" class="mr-3" id="edit"  data-id="${contact.id}">
              <i class="fas fa-pencil-alt"></i>
            </a>
            <a href="#" id="delete" data-id="${contact.id}">
              <i class="fas fa-trash-alt"></i>
            </a>
          </div>
        </div>
      `;
    });
    this.contactInput.innerHTML = output;
  }
  fillForm({ firstName, lastName, email, phone, id }) {
    this.firstNameInput.value = firstName;
    this.lastNameInput.value = lastName;
    this.emailInput.value = email;
    this.phoneInput.value = phone;
    this.idInput.value = id;
  }
  editBtnHandle() {
    this.contactSubmit.textContent = 'Update Contact';
    this.contactSubmit.classList.remove('btn-primary');
    this.contactSubmit.classList.add('btn-danger');
    //adding cancelBtn in edit
    this.addCancelBtn();
  }

  addCancelBtn() {
    const btn = document.createElement('button');
    btn.className = 'btn btn-block btn-success';
    btn.id = 'cancel';
    btn.textContent = 'Back';
    //inserting cancel button before form ending
    this.contactForm.insertBefore(btn, this.FormEnd);
  }
  changeState(state) {
    if (state === 'add') {
      if (document.getElementById('cancel')) {
        document.getElementById('cancel').remove();
      }
      this.contactSubmit.textContent = 'Submit Contact';
      this.contactSubmit.classList.add('btn-primary');
      this.contactSubmit.classList.remove('btn-danger');
      this.idInput.value = '';
    }
  }
  clearFields() {
    this.firstNameInput.value = '';
    this.lastNameInput.value = '';
    this.emailInput.value = '';
    this.phoneInput.value = '';
  }
  showAlert(msg, className) {
    const div = document.createElement('div');
    div.className = `alert ${className}`;
    div.innerHTML = msg;
    this.containerForm.insertBefore(div, this.contactInput);
    setTimeout(() => {
      this.clearAlert();
    }, 2000);
  }
  clearAlert() {
    if (document.querySelector('.alert')) {
      document.querySelector('.alert').remove();
    }
  }
}

export const ui = new Ui();
