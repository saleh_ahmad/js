//importing http module
import { http } from './http';
//import ui module
import { ui } from './ui';
//Getting contacts after DoM being Loaded
document.addEventListener('DOMContentLoaded', getContacts);
//Delete contact
document.getElementById('contacts').addEventListener('click', deleteContact);
//Edit contact
document.getElementById('contacts').addEventListener('click', editContact);
//Form submit handle
document
  .getElementById('contact-submit')
  .addEventListener('click', submitContact);
//Handling cancel button
document.querySelector('.contact-form').addEventListener('click', cancelUpdate);
//edit contact
function editContact(evt) {
  //checking id of parent
  if (evt.target.parentElement.id === 'edit') {
    //Getting id
    const id = evt.target.parentElement.dataset.id;
    //Getting contacts
    http.get(`http://localhost:3000/contacts/${id}`).then(data => {
      //filling from data
      ui.fillForm(data);
      //Handling button of edit state
      ui.editBtnHandle();
    });
  }
}
//handling cancel update
function cancelUpdate(e) {
  //checking id of parent
  if (e.target.id === 'cancel') {
    //changing form to add state
    ui.changeState('add');
    //clearing Form filed after clicking cancel button
    ui.clearFields();
  }
  e.preventDefault();
}
function deleteContact(evt) {
  //checking id of parent
  if (evt.target.parentElement.id === 'delete') {
    //Getting id
    const id = evt.target.parentElement.dataset.id;
    //Deleting contact
    http
      .delete(`http://localhost:3000/contacts/${id}`)
      .then(() => {
        //showing message in UI(user interface)
        ui.showAlert('contact Deleted', 'alert-success');
        //Getting contacts
        getContacts();
      })
      .catch(err => ui.showAlert("contact can't be Deleted", 'alert-success'));
  }
}

function submitContact(e) {
  //preventing Browser reloading after form submit
  e.preventDefault();
  //Getting value of form field
  const firstName = document.getElementById('firstName').value;
  const lastName = document.getElementById('lastName').value;
  const email = document.getElementById('email').value;
  const phone = document.getElementById('phone').value;
  const id = document.getElementById('id').value;
  //checking Form validation
  if (firstName === '' || lastName === '' || email === '' || phone === '') {
    //Showing message to UI
    ui.showAlert('Please fill up the necessary field', 'alert-danger');
  } else {
    //constructing data object from form field value
    const data = {
      firstName,
      lastName,
      email,
      phone
    };
    //checking state of form - id is empty string in add state
    if (id === '') {
      //sending data to the server
      http.post('http://localhost:3000/contacts', data).then(() => {
        //showing message
        ui.showAlert('contact Added', 'alert-success');
        //clearing form field after sending data to server
        ui.clearFields();
        //Getting contacts
        getContacts();
      });
    } else {
      //updating contacts in the server
      http.update(`http://localhost:3000/contacts/${id}`, data).then(() => {
        //showing message
        ui.showAlert('contact updated', 'alert-success');
        //clearing form field after updating  data in server
        ui.clearFields();
        ui.changeState('add');
        //Getting contacts
        getContacts();
      });
    }
  }
}

function getContacts() {
  //Getting contacts
  http
    .get('http://localhost:3000/contacts')
    .then(contacts => {
      //Showing data in UI
      ui.paint(contacts);
    })
    .catch(() =>
      //showing message
      ui.showAlert('problem in getting contacts', 'alert-danger')
    );
}
