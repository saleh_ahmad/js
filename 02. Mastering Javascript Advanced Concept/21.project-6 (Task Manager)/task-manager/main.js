//function to  do storage related task
const StorageCtrl = (function() {
  return {
    addTask(task) {
      let tasks;
      if (localStorage.getItem('tasks') === null) {
        tasks = [];
        tasks.push(task);
      } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
        tasks.push(task);
      }
      localStorage.setItem('tasks', JSON.stringify(tasks));
    },
    getTasks() {
      let tasks;
      if (localStorage.getItem('tasks') === null) {
        tasks = [];
      } else {
        tasks = JSON.parse(localStorage.getItem('tasks'));
      }
      return tasks;
    },
    updateTask(updatedTask) {
      console.log(updatedTask);
      let tasks = JSON.parse(localStorage.getItem('tasks'));
      const taskAfterUpdates = tasks.map(task => {
        if (task.id === updatedTask.id) {
          task.title = updatedTask.title;
          task.completed = updatedTask.completed;
          return task;
        } else {
          return task;
        }
      });
      localStorage.setItem('tasks', JSON.stringify(taskAfterUpdates));
    },
    deleteTask(taskToDelete) {
      let tasks = JSON.parse(localStorage.getItem('tasks'));
      const taskAfterDeletion = tasks.filter(
        task => task.id !== taskToDelete.id
      );
      tasks = taskAfterDeletion;
      localStorage.setItem('tasks', JSON.stringify(tasks));
    }
  };
})();

//Function to do control data related task
const TaskCtrl = (function() {
  let data = {
    tasks: StorageCtrl.getTasks(),
    currentTask: null
  };

  return {
    getTasks() {
      return data.tasks;
    },
    getTaskById(id) {
      return data.tasks.find(task => task.id === id);
    },
    setCurrentTask(taskToEdit) {
      data.currentTask = taskToEdit;
    },
    getCurrentTask() {
      return data.currentTask;
    },
    updateItem(taskTitle) {
      let foundTask = null;
      data.tasks = data.tasks.map(task => {
        if (task.id === data.currentTask.id) {
          task.title = taskTitle;
          foundTask = task;
          return task;
        } else {
          return task;
        }
      });
      return foundTask;
    },
    addTask(taskTitle) {
      const id =
        data.tasks.length > 0 ? data.tasks[data.tasks.length - 1].id + 1 : 0;
      const task = {
        id,
        title: taskTitle,
        completed: false
      };
      //using object destructuring practically
      const dataWithUpdatedTask = {
        ...data,
        tasks: [...data.tasks, task]
      };
      data = dataWithUpdatedTask;
      return task;
    },
    getTotalTaskCount() {
      return data.tasks.length;
    },
    getCompletedTaskCount() {
      return data.tasks.reduce((acc, currentTask) => {
        if (currentTask.completed) {
          ++acc;
          return acc;
        } else {
          return acc;
        }
      }, 0);
    },
    deleteTask(currentTask) {
      const tasksAfterDeletion = data.tasks.filter(
        task => task.id !== currentTask.id
      );
      data.tasks = tasksAfterDeletion;
    },
    completedTask(id) {
      data.tasks = data.tasks.map(task => {
        if (task.id === id) {
          task.completed = !task.completed;
          return task;
        } else {
          return task;
        }
      });
    },
    getData() {
      return data;
    }
  };
})();

//function to  do UI related task
const UICtrl = (function() {
  const selectors = {
    taskContainer: '.task-container',
    addTask: '.add-task',
    updateTask: '.update-task',
    deleteTask: '.delete-task',
    backBtn: '.back-btn',
    taskTitle: '.task-title',
    completedTask: '.completed-tasks',
    totalTasks: '.total-tasks',
    alert: '.alert'
  };

  const hideTaskContainer = function() {
    document.querySelector(selectors.taskContainer).style.display = 'none';
  };
  const showTaskContainer = function() {
    document.querySelector(selectors.taskContainer).style.display = 'block';
  };

  return {
    getSelectors() {
      return selectors;
    },
    showEditState() {
      document.querySelector(selectors.addTask).style.display = 'none';
      document.querySelector(selectors.updateTask).style.display =
        'inline-block';
      document.querySelector(selectors.deleteTask).style.display =
        'inline-block';
      document.querySelector(selectors.backBtn).style.display = 'inline-block';
    },
    clearEditState() {
      document.querySelector(selectors.addTask).style.display = 'inline-block';
      document.querySelector(selectors.updateTask).style.display = 'none';
      document.querySelector(selectors.deleteTask).style.display = 'none';
      document.querySelector(selectors.backBtn).style.display = 'none';
    },
    getTitleInput() {
      return document.querySelector(selectors.taskTitle).value;
    },
    showAlert(msg, className) {
      //creating div
      const div = document.createElement('div');
      //setting message
      div.textContent = msg;
      //setting class name
      div.className = className;
      //inserting div in DOM
      document
        .querySelector(selectors.taskContainer)
        .insertAdjacentElement('beforebegin', div);
      //Removing alert after specific time
      if (document.querySelector(selectors.alert)) {
        this.clearAlert();
      }
    },
    clearAlert() {
      setTimeout(() => {
        document.querySelector(selectors.alert).remove();
      }, 2000);
    },
    clearFields() {
      document.querySelector(selectors.taskTitle).value = '';
    },
    populateForm(taskTitle) {
      document.querySelector(selectors.taskTitle).value = taskTitle;
    },
    showTotalTaskCount(tasksCount) {
      document.querySelector(selectors.totalTasks).textContent = tasksCount;
    },
    showCompletedTaskCount(tasksCount) {
      document.querySelector(selectors.completedTask).textContent = tasksCount;
    },
    populateTask(task) {
      const { id, title } = task;
      showTaskContainer();
      let output = '';
      output += `
      <div class="task-item mb-2" id="task-${id}">
        <div class="row">
          <div class="col-sm-6">
            <h5>${title}</h5>
          </div>
          <div class="col-sm-6">
            <a href="#" class="completed-task float-right">
              <i class="fas fa-check"></i>
            </a>
            <a href="#" class="edit-task float-right mr-3">
              <i class="fas fa-pencil-alt"></i>
            </a>
          </div>
        </div>
      </div>
      
      `;
      //inserting into DOM
      document
        .querySelector(selectors.taskContainer)
        .insertAdjacentHTML('beforeend', output);
    },
    populateTasks(tasks) {
      if (tasks.length === 0) {
        //Hiding task container if there is no tasks
        hideTaskContainer();
      } else {
        //showing task container if there is a task
        showTaskContainer();
        let output = '';
        tasks.forEach(({ id, title, completed }) => {
          output += `
        <div class="task-item mb-2" id="task-${id}">
          <div class="row">
            <div class="col-sm-6">
              <h5 class=${
                completed === true ? 'completed-task' : ''
              }>${title}</h5>
            </div>
            <div class="col-sm-6">
              <a href="#" class="completed-task float-right">
                <i class="fas fa-check"></i>
              </a>
              <a href="#" class="edit-task float-right mr-3">
                <i class="fas fa-pencil-alt"></i>
              </a>
            </div>
          </div>
        </div>
        `;
        });
        //inserting into DOM
        document.querySelector(selectors.taskContainer).innerHTML = output;
      }
    }
  };
})();

//function to  do connection between different part
const AppCtrl = (function(TaskCtrl, UICtrl, StorageCtrl) {
  const loadEventListener = function() {
    //getting selectors
    const selectors = UICtrl.getSelectors();
    //Register event listener to add task button
    document
      .querySelector(selectors.addTask)
      .addEventListener('click', TaskAddSubmit);
    //Register event listener to update task button
    document
      .querySelector(selectors.updateTask)
      .addEventListener('click', updateTaskSubmit);
    //Register event listener to delete task button
    document
      .querySelector(selectors.deleteTask)
      .addEventListener('click', deleteTaskSubmit);
    //Register event listener to back to task button
    document
      .querySelector(selectors.backBtn)
      .addEventListener('click', backToAddTaskState);
    //Register event listener to completed task icon
    document
      .querySelector(selectors.taskContainer)
      .addEventListener('click', completedTask);
    //Register event listener to edit task icon
    document
      .querySelector(UICtrl.getSelectors().taskContainer)
      .addEventListener('click', editTask);
  };

  function TaskAddSubmit(e) {
    e.preventDefault();
    const taskTitle = UICtrl.getTitleInput();
    if (taskTitle.trim() === '') {
      UICtrl.showAlert(
        'please provide necessary information',
        'alert alert-danger'
      );
    } else {
      //update to data storage
      const task = TaskCtrl.addTask(taskTitle);
      //update to local storage
      StorageCtrl.addTask(task);
      //clear from form field
      UICtrl.clearFields();
      //get total task count
      const totalTasks = TaskCtrl.getTotalTaskCount();
      //update total task count
      UICtrl.showTotalTaskCount(totalTasks);
      //update to Ui
      UICtrl.populateTask(task);
    }
  }
  function completedTask(e) {
    if (e.target.parentElement.classList.contains('completed-task')) {
      //getting full id
      const targetId =
        e.target.parentElement.parentElement.parentElement.parentElement.id;
      //getting id in number format
      const id = Number(targetId.split('-')[1]);
      //update completed property in data storage
      TaskCtrl.completedTask(id);
      //Get task by Id
      const updatedTask = TaskCtrl.getTaskById(id);
      //Save updated task to localStorage
      StorageCtrl.updateTask(updatedTask);
      //get completed task count
      const completedTaskCounts = TaskCtrl.getCompletedTaskCount();
      //update UI
      UICtrl.showCompletedTaskCount(completedTaskCounts);

      //Getting tasks
      const tasks = TaskCtrl.getTasks();
      //update UI
      UICtrl.populateTasks(tasks);
    }
  }
  function editTask(e) {
    if (e.target.parentElement.classList.contains('edit-task')) {
      const targetId =
        e.target.parentElement.parentElement.parentElement.parentElement.id;
      //show Edit state
      UICtrl.showEditState();
      //getting id
      const id = Number(targetId.split('-')[1]);
      //getting task by Id
      const taskToUpdate = TaskCtrl.getTaskById(id);
      //update state in  data storage
      TaskCtrl.setCurrentTask(taskToUpdate);
      //populate form in Edit state
      UICtrl.populateForm(taskToUpdate.title);
    }
  }

  function backToAddTaskState(e) {
    //prevent default action
    e.preventDefault();
    //clear edit state
    UICtrl.clearEditState();
    //clear form
    UICtrl.clearFields();
  }

  function deleteTaskSubmit(e) {
    //prevent default action
    e.preventDefault();
    //get EditedTask/currentTask
    const currentTask = TaskCtrl.getCurrentTask();
    //Deleted from data storage
    TaskCtrl.deleteTask(currentTask);
    //Delete task from local storage
    StorageCtrl.deleteTask(currentTask);

    //clear form field
    UICtrl.clearFields();
    //clear edit state
    UICtrl.clearEditState();
    //get tasks
    const tasks = TaskCtrl.getTasks();
    //update UI
    UICtrl.populateTasks(tasks);
  }
  function updateTaskSubmit(e) {
    e.preventDefault();
    //get Input value
    const titleInput = UICtrl.getTitleInput();
    //updating input to our data storage
    const updatedTask = TaskCtrl.updateItem(titleInput);
    //updating task to localStorage
    console.log(updatedTask);
    StorageCtrl.updateTask(updatedTask);
    //clear form field
    UICtrl.clearFields();
    //clear edit state
    UICtrl.clearEditState();
    //get tasks
    const tasks = TaskCtrl.getTasks();
    //update UI
    UICtrl.populateTasks(tasks);
  }
  return {
    init() {
      //get total task count
      const totalTasks = TaskCtrl.getTotalTaskCount();
      UICtrl.showTotalTaskCount(totalTasks);
      //get completed task count
      const completedTaskCounts = TaskCtrl.getCompletedTaskCount();
      UICtrl.showCompletedTaskCount(completedTaskCounts);
      //Getting tasks from data center
      const tasks = TaskCtrl.getTasks();
      //populating  task in UI
      UICtrl.populateTasks(tasks);
      //clear edit state
      UICtrl.clearEditState();
      //calling Event listener
      loadEventListener();
    }
  };
})(TaskCtrl, UICtrl, StorageCtrl);

//calling init function(executed initially after browser load)
AppCtrl.init();
