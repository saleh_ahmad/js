//A design pattern is a general repeatable solution to a commonly occurring problem in software design

//Modular design pattern
//facade design pattern
//Prototype Design Pattern.
//Observer Design Pattern.
// Singleton
//mediator

//Modular Design pattern
const profile = (function() {
  const firstName = 'samim';
  const lastName = 'Hasan';
  const fullName = function() {
    return firstName + ' ' + lastName;
  };
  return {
    getFullName: function() {
      const initials = 'MD ';
      return initials + fullName();
    }
  };
})();

console.log(profile.getFullName());
