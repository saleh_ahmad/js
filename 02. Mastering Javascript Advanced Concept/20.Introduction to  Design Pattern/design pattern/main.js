//scope : where a variable(?) is accessible
//searching: inside  to outside (scope chain)
//* Not outside to inside

//Scope review
// const firstName = 'Khalil';

// function getProfile() {
//   //const firstName = 'samim';
//   console.log(firstName);
//   function anotherScope() {
//     const firstName = 'samim';
//     //console.log(firstName);
//   }
//   anotherScope();
// }

// console.log(firstName);
// console.log(getProfile());

//closure explained- A closure is a feature in JavaScript where an inner function has access to the outer (enclosing) function's variables
// function myOuterFunction() {
//   const myVar = 'blah';
//   function myInnerFunction() {
//     console.log(myVar);
//   }
//   return myInnerFunction;
// }
// const myClosure = myOuterFunction();
// console.log(myClosure());
