const btn = document.querySelector('button');
// btn.addEventListener('click', loadData);
// function loadData(e) {
// const xhr = new XMLHttpRequest();

// you can use spinners or loaders
// xhr.onprogress = function() {
//   console.log(this.readyState);
// };

// xhr.onload = function() {

// readyState	Holds the status of the XMLHttpRequest state
// 0: request not initialized
// 1: server connection established
// 2: request received
// 3: processing request
// 4: request finished and response is ready

//HTTP most common status code
// status	200: "OK"
// 403: "Forbidden"
// 404: "Page not found"

// console.log('state', this.readyState);
// console.log('status', this.status);
//   if (this.status === 200) {
//     document.querySelector('.data').textContent = JSON.parse(
//       this.responseText
//     );
//     console.log(JSON.parse(this.responseText));
//   }
// };

//you can also send, POST, PUT, DELETE Request
//In case of post, put request you must have to send related data with xhr.send()
//more on -https://www.w3schools.com/XML/ajax_xmlhttprequest_send.asp

// xhr.open('GET', './sample.json', true);
// xhr.send();
// }

//handling promise with then for loading data
//Know more about fetch - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
// function loadData() {
//   fetch('./sample.json')
//     .then(data => data.json())
//     .then(data => console.log(data));
//  console.log(result);
// }

//using async await to handle promise for loading data
// async function loadData() {
//   const result = await fetch('./sample.json').then(data => data.json());
//   console.log(result);
// }
