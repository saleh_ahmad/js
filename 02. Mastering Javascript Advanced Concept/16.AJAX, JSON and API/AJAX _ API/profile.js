const output = document.querySelector('.output');
const btn = document.querySelector('button');

btn.addEventListener('click', loadProfiles);

async function loadProfiles() {
  //get profiles data from sample.json
  const profiles = await fetch('./sample.json').then(data => data.json());
  //html after constructing profile data
  let resultHTML = '';
  profiles.forEach((profile, index) => {
    const { firstName, lastName, age, programming } = profile;
    resultHTML += `
      <h3> profile - ${index + 1}</h3>
      <hr/>
      <ul>
        <li>${firstName}</li>
        <li>${lastName}</li>
        <li>${age}</li>
        <li>${programming}</li>
      </ul>
    `;
  });
  //Rendering to dom
  output.innerHTML = resultHTML;
}
