//GET - Reading /Getting Data
//POST- posting/ adding data
//PUT- updating data
//PATCH -updating data
//DELETE - Deleting data

const output = document.querySelector('.output');
const btn = document.querySelector('button');

btn.addEventListener('click', getJoke);
async function getJoke() {
  const joke = await fetch(
    'https://api.chucknorris.io/jokes/random?category=food'
  ).then(data => data.json());
  output.textContent = joke.value;
}

//getting categories list
// fetch('https://api.chucknorris.io/jokes/categories')
//   .then(data => data.json())
//   .then(category => console.log(category));

// const http = new Http();
// http
//   .get('https://api.chucknorris.io/jokes/categories')
//   .then(result => console.log(result));
