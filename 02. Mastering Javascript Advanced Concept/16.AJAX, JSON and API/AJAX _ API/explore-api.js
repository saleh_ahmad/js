//GET - Getting multiple todos
// async function getTodos() {
//   const todos = await fetch('https://jsonplaceholder.typicode.com/todos').then(
//     data => data.json()
//   );
//   console.log(todos);
// }
// getTodos();

//GET- Getting single todo
// async function getTodo() {
//   const todo = await fetch('https://jsonplaceholder.typicode.com/todos/1').then(
//     data => data.json()
//   );
//   console.log(todo);
// }
// getTodo();

// const data = {
//   title: 'Hello world',
//   completed: true,
//   userId: 100
// };

//posting new todo
// async function postTodo() {
//   const todo = await fetch('https://jsonplaceholder.typicode.com/todos', {
//     method: 'POST',
//     body: JSON.stringify(data),
//     headers: {
//       'Content-type': 'application/json; charset=UTF-8'
//     }
//   }).then(data => data.json());
//   console.log(todo);
// }
// postTodo();

// const upDateData = {
//   title: 'Hello world Again',
//   completed: true,
//   userId: 100
// };
//update a existing todo

// async function updateTodo() {
//   const todo = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
//     method: 'PUT',
//     body: JSON.stringify(upDateData),
//     headers: {
//       'Content-type': 'application/json; charset=UTF-8'
//     }
//   }).then(data => data.json());
//   console.log(todo);
// }
// updateTodo();

//Deleting a existing todo

// async function deleteTodo() {
//   const todo = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
//     method: 'DELETE'
//   }).then(data => data.json());
//   console.log(todo);
// }
// deleteTodo();

const http = new Http();
//GET request - get data from API
http
  .get('https://jsonplaceholder.typicode.com/todos')
  .then(result => console.log(result));

const data = {
  title: 'Hello world',
  completed: true,
  userId: 100
};
//POST request - posting new data(adding data to the destination(API))
http
  .post('https://jsonplaceholder.typicode.com/todos', data)
  .then(result => console.log(result));

//PUT REQUEST - update data
const updateData = {
  title: 'Hello world Again',
  completed: true,
  userId: 100
};

http
  .update('https://jsonplaceholder.typicode.com/todos/1', updateData)
  .then(result => console.log(result));

//DELETE REQUEST -deleting  specific data
http
  .delete('https://jsonplaceholder.typicode.com/todos/1')
  .then(result => console.log(result));

//Heart of Every application
// C = create
// R = Read
// U = Update
// D = Delete
