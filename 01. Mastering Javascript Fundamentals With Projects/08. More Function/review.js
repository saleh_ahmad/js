//Function Statement
function greet(name) {
    return `Hi ${name}`;
}

console.log(greet('Saleh'));

//Function Expression
const greetings = function(name) {
    return `Hi ${name}`;
}

console.log(greetings('Saleh'));
