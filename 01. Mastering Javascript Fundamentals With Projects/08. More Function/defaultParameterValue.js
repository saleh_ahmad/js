function product(name, price, discount = 0.1, shipping, category='T-Shirt') {
    return [name, price, discount, shipping, category];
}

const shirt = product('Printed T-Shirt', 30, undefined, true);
console.log(shirt);
