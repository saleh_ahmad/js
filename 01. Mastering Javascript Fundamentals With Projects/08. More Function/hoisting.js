/*
 * preparation stage:
 * var firstName,
 * var lastName,
 * function greetS() {
 *     console.log("Hi");
 * }
 * var greetE
 *
 * Execution stage:
 * firstName = 'samim';
 * lastName = 'Hasan';
 * greetS()
 * greetE()
 */

console.log(firstName);

const firstName = "samim";
const lastName = "Hasan";
console.log(firstName);
console.log(lastName);
function greetS() {
  console.log("Hi");
}
greetS();

greetE();

const greetE = () => {
  console.log("Hi");
};
greetE();