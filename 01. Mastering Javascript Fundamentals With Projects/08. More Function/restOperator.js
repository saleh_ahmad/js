//rest operator
function product(name, ...property) {
    console.log(name);
    console.log(property);

    let info = '';
    for (const property of arguments) {
        console.log(property);
        info += property + ' ';
    }

    console.log(info);

    return `${name} - $${property[0]}`;
}

const shirt = product('Printed T-Shirt', 30, 0, true, 'T-Shirt Category');
console.log(shirt);