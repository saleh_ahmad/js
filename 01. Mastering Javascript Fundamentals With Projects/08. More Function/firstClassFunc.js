//return an object from inside an object
const product = {
    name : 'T-Shirt',
    price : '$10',
    //showInfo : () => `${this.name} - ${this.price}`
    showInfo() {
        return {
            name : this.name,
            price : this.price
        };
    }
};

const greet = () => {
    console.log('Hi');
}

greet();

//callback function (return function)
const greet2 = () => {
    return function() {
        return 'Hi';
    }
}
console.log(greet2()());

//================== Function as a Parameter =========================
const greet3 = (fn) => {
    console.log(fn());
}

greet3(function() {
    return 'Hi';
});