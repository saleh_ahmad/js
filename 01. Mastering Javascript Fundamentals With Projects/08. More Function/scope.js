//Global Scope
const firstName = 'Ahmad';
let lName = 'Ahmad'

//Functional Scope
function logger() {
    lName = 'Sayeed';
    //const firstName = 'Saleh';

    //Inner Scope
    return loggerInner = () => {
        //const firstName = 'Abdul';
        console.log(firstName);
    }
}

logger()();
console.log(firstName);
console.log(lName);