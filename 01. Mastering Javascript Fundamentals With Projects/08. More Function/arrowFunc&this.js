//Arrow function will not work for this keyword
const product = {
    name : 'T-Shirt',
    price : '$10',
    //showInfo : () => `${this.name} - ${this.price}`
    showInfo() {
        return `${this.name} - ${this.price}`;
    }
};

console.log(product.showInfo());