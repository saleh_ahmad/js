//let, const -> only accessible in curly braces
{
    let fName = 'Saleh';
}

//console.log(fName);

//With var
for (var i = 0; i < 10; ++i) {
    console.log(i);
}

console.log(i);

//with let
for (let j = 0; j < 10; ++j) {
    console.log(j);
}

//console.log(j);

var variable = 'bla bla';
var variable = 'bla';

console.log(variable);

//Will got an error message that it has been already declared
let variable = 'bla bla';
let variable = 'bla';

console.log(variable);