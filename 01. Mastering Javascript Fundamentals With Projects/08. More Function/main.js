//1.FUnction review
//=====================================================

//Function statement
// function greet(name){
//   return 'hi ' + name;
// }

//Function expression
// const greet = function(name){
//   return 'hi ' + name;
// }
// greet.lastName = 'Hasan';

// console.log(greet('samim'));
// console.log(greet.lastName)

//2.arguments 3.Rest operator
//============================================================
// function product(name,...property) {
//   console.log(name)
//   console.log(property)
//   return `
//   ${name}-$${property[0]}
//   `
//   // let info = "";
//   // for (const property of arguments) {
//   //   info += property + " ";
//   // }
//   // return info;
//   // return `${name}-$${price}`
// }

//4.Default parameter
//=============================================================

// function product(name, price, discount = 0.1, shipping, category = "T-shirt") {
//   console.log(name, price, discount, shipping, category);
// }

// const shirt = product("printed T-shirt", 30, undefined, true);
// console.log(shirt);

//5.Arrow function and evolution
//==============================================
// function add(num1, num2){
//  return num1 + num2
// }

// const add = (num1, num2) =>num1 + num2
// const result = add(3, 2)
// console.log(result)

// const multiply = num => num * num
// const result = multiply(3)
// console.log(result)

//6.Arrow function and this binding
//=============================================================

// const product = {
//   name:'T-shirt',
//   price: '$10',
//   showInfo() {
//     console.log(this);
//     return `${this.name}-${this.price}`;
//   }
// }

// const info = product.showInfo();
// console.log(info);

//7.Array destructuring
//====================================================

// const product = ['T-shirt', 10, 0, true];

// const name = product[0];
// const price = product[1];
// const discount = product[2];

//Array destructuring
// const [name, price, ...others] = product
// console.log(name, price, others)

//Object destructuring
//===================================================

// const product = {
//   name: 'T-shirt',
//   price: '$10',
//   showInfo() {
//     console.log(this);
//     return `${this.name}-${this.price}`;
//   }
// }

// const name = product.name;
// const price = product.price;
// console.log(name, price)

// const { name, price} = product
// console.log(name, price)

// function productOut({ name, price, showInfo }) {
//   console.log(name, price);
//   // console.log(name, price, discount, shipping, category);
// }
// productOut(product);

//8.First class function
//=====================================================================
//First-class functions when functions are treated like any other variable. For example, in Javascript, a function can be passed as an argument to other functions, can be returned by another function and can be assigned as a value to a variable.

//Higher order function
//a higher-order function is a function that does at least one of the following: takes one or more functions as arguments, returns a function as its result.

//callback function
//A callback function is a function passed into another function as an argument, which is then invoked inside the outer function to complete some kind of routine or action

// function greet(){
//   console.log('Hi');
// }

//greet();

// const greet = () => {
//   console.log('Hi');
//}

// const greet = () => {
//   return function(){
//     return 'Hi';
//   }
// }

// console.log(greet()())

// const greet = (fn) => {
//   console.log(fn());
// }
// greet(function(){
//   return 'Hi';
// })

//9.Difference between function statement and expression
//================================================
//console.log(firstName)
// const firstName = "samim";
// const lastName = "Hasan";
// console.log(firstName);
// console.log(lastName);
// function greetS() {
//   console.log("Hi");
// }
// greetS();
// greetE()
// const greetE = () => {
//   console.log("Hi");
// };
// greetE();
// var firstName = 'samim'

//11.scope
//Functional scope
//=============================================================
// function logger(){
//    firstName = 'Anis';
//   //console.log(lastName)
//   return function loggerInner(){
//    //const firstName = 'Anis';
//    //const lastName = 'Hasan';
//    console.log(firstName);
//   }
//   console.log(firstName);
// }
// logger()();
// console.log(firstName)

//12.Difference between var vs let and const
// Now You can defined scope by using {} and let and const
//=======================================================
//

// {
//   let firstName = 'samim';
//   console.log(firstName);

// }

// for(let i = 0; i < 10; i++){
//   console.log(i)
// }
//console.log(i)

//console.log(firstName);

// let someVar = 'blabla';
// let someVar = 'bla';
// console.log(someVar);

//13.Difference between rest and spread operator
//===================================================

// function product(name,...property) {
//   console.log(name)
//   console.log(property)
//   return `
//   ${name}-$${property[0]}
//   `
// let info = "";
// for (const property of arguments) {
//   info += property + " ";
// }
// return info;
// return `${name}-$${price}`
//}
// product("printed T-shirt", 30, undefined, true);

// const profile = ['samim', 'Hasan', 25, 'web development'];

// const copyProfile = [...profile];

// console.log(copyProfile);

// const product = {
//   name: 'T-shirt',
//   price: '$10',
//   showInfo() {
//     console.log(this);
//     return `${this.name}-${this.price}`;
//   }
// }
// function productOut({name, price}) {
//   console.log(name, price)
//   //console.log(name, price);
//   // console.log(name, price, discount, shipping, category);
// }
// productOut({...product});//Not passing exact product rather a copy
