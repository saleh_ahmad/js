/*
 * Function Statement
 * It can be defined before/after call
 */
console.log(sum(7, 3));
function sum(num1, num2) {
    return num1 + num2;
}
console.log(sum(2, 3));


/*
 * Function Expression
 * It can be defined only before call
 */
console.log(add(2, 3));
const add = (num1, num2) => {
    return num1 + num2;
}
console.log(add(2, 3));