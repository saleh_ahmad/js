//===== Function Statement ===============================
function sum(num1, num2) {
    return num1 + num2;
}

console.log(sum(2, 3));

//===== Function Expression ==============================
const add = function(num1, num2) {
    return num1 + num2;
}
console.log(add(2, 3));

/*
 * Without function keyword
 * Arrow Function
 */
const addition = (num1, num2) => {
    return num1 + num2;
}
console.log(addition(2, 3));

/*
 * Single line statement
 * no return keyword
 * no curly braces
 */
const summation = (num1, num2) => num1 + num2;
console.log(summation(2, 3));

/*
 * Single Parameter
 * no need of left & right parenthesis
 */
const multiply = num => num * num;
console.log(multiply(3));