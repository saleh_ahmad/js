//Array Destructuring
const product = ['T-shirt', 10, 0, 2];

const [name, price, discount, shipping] = product;
console.log(name, price, discount, shipping);

//Rest Operator
const [name2, price2, ...others] = product;
console.log(name, price, others);

//Object Destructuring
const productObj = {
    name3 : 'T-Shirt',
    price3 : '$10',
    showInfo() {
        return `${this.name} - ${this.price}`;
    }
};

function productOut({name3, price3, showInfo}) {
    console.log(name3, price3);
}

productOut(productObj);

//Rest Operator
const {name3, price3, ...others3} = productObj;
console.log(name3, price3, others3);