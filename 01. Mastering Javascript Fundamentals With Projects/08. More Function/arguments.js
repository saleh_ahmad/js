function product(name, price, discount, shipping) {
    console.log(arguments);

    let info = '';
    for (const property of arguments) {
        console.log(property);
        info += property + ' ';
    }

    console.log(info);

    return `${name} - $${price}`;
}

const shirt = product('Printed T-Shirt', 30, 0, true);
console.log(shirt);