//Count the truthy value;
const countTruthy = arr => {
    let count = 0;

    for (key of arr) {
        if (key) {
            count++;
        }
    }

    return count;
}

const array = [0, null, undefined, '', 2, 3];
console.log(countTruthy(array));

//Add those numbers
const sum = (...property) => {
    if (!Array.isArray(property[0])) {
        return 'Only Array is accepted.';
    }

    let sum = 0;
    for (key of property[0]) {
        sum += key;
    }

    return sum;
}

const numbers = [1, 2, 3, 4];
console.log(sum(numbers));
