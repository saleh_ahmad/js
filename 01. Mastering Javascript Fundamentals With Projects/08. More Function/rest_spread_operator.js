//rest operator
function product(...property) {
    console.log(property);

    let info = '';
    for (const property of arguments) {
        console.log(property);
        info += property + ' ';
    }

    console.log(info);

    return `${property[0]} - $${property[1]}`;
}

const shirt = product('Printed T-Shirt', 30, 0, true, 'T-Shirt Category');
console.log(shirt);


//spread operator
const product1 = {
    name : 'T-Shirt',
    price : 20,
    availability : true
};
const product4 = {...product1};
console.log(product4);

const profile = ['Saleh', 'Ahmad', 27, 'Web Developer'];
const profile2 = [...profile];
console.log(profile2);

function myProduct(product) {
    console.log(`${product.name} - ${product.price}`);
}
myProduct({...product1});

function showProfile(fName, lName) {
    console.log(fName, lName);
}
showProfile(...profile);