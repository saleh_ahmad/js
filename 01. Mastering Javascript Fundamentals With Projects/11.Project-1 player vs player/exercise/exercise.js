const guessMessage = document.querySelector('#guess-msg'),
    guessedNumber = document.querySelector('#guessedNumber'),
    p1Btn = document.querySelector('#p1Btn'),
    p2Btn = document.querySelector('#p2Btn'),
    resetBtn = document.querySelector('#resetBtn');

let minNumber = 1,
    maxNumber = 10,
    randomNumber = 0,
    maxTries = 3,
    p1Tries = p2Tries = 0,
    gameOver = false;

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min;
}

function reset() {
    p1Tries = p2Tries = 0;
    gameOver = false;

    randomNumber = getRandomInt(minNumber, maxNumber);

    guessedNumber.value = '';

    p1Btn.removeAttribute('disabled');
    p2Btn.removeAttribute('disabled');

    guessMessage.classList.remove('text-success', 'text-danger');
    guessMessage.classList.add('d-none');
    guessMessage.textContent = '';
    guessedNumber.value = '';
}

function winner(player, tries) {
    const playerName = (player === 'p1') ? 'Player 1' : 'Player 2';
    if (tries > maxTries) {
        return;
    }

    guessMessage.classList.remove('d-none');
    if (Number(guessedNumber.value) === randomNumber) {
        gameOver = true;

        guessMessage.classList.remove('text-danger');
        guessMessage.classList.add('d-block', 'text-success');
        guessMessage.textContent = `Congratulations ${playerName}!, Your guess is correct!!`;
    } else {
        guessMessage.classList.remove('text-success');
        guessMessage.classList.add('d-block', 'text-danger');
        guessMessage.textContent = `${playerName}, Your guess is wrong!!`;
    }
}

randomNumber = getRandomInt(minNumber, maxNumber);
//console.log(randomNumber);

//addEventListener
p1Btn.addEventListener('click', () => {
    if (gameOver) {
        return;
    }

    p1Tries++;

    if (p1Tries === maxTries) {
        p1Btn.setAttribute('disabled', 'disabled');
    }

    winner('p1', p1Tries);
});

p2Btn.addEventListener('click', () => {
    if (gameOver) {
        return;
    }

    p2Tries++;

    if (p2Tries === maxTries) {
        p2Btn.setAttribute('disabled', 'disabled');
    }

    winner('p2', p2Tries);
});

resetBtn.addEventListener('click', reset);
