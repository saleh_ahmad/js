//Variables
const p1ScoreDisplay = document.querySelector('#p1Score'),
    p2ScoreDisplay = document.querySelector('#p2Score'),
    winningScoreDisplay = document.querySelector('p span'),
    inputScore = document.querySelector('#inputScore'),
    p1Btn = document.querySelector('#p1Btn'),
    p2Btn = document.querySelector('#p2Btn'),
    resetBtn = document.querySelector('#resetBtn');

let p1Score = 0,
    p2Score = 0,
    winningScore = 5,
    gameOver = false;

//Functions
function winner(oldScore, winningScore) {
    if (oldScore === winningScore) {
        p1Score === winningScore
            ? p1ScoreDisplay.classList.add('winner')
            : p2ScoreDisplay.classList.add('winner');

        gameOver = true;
        p1Btn.setAttribute('disabled', 'disabled');
        p2Btn.setAttribute('disabled', 'disabled');
    }
}

function reset() {
    p1Score = p2Score = 0;
    gameOver = false;

    p1ScoreDisplay.textContent = p1Score;
    p2ScoreDisplay.textContent = p2Score;

    p1ScoreDisplay.classList.remove('winner');
    p2ScoreDisplay.classList.remove('winner');

    p1Btn.removeAttribute('disabled');
    p2Btn.removeAttribute('disabled');
}

//addEventListener
p1Btn.addEventListener('click', () => {
    if (!gameOver) {
        p1Score++;
        p1ScoreDisplay.textContent = p1Score;
    }

    winner(p1Score, winningScore);
});

p2Btn.addEventListener('click', () => {
    if (!gameOver) {
        p2Score++;
        p2ScoreDisplay.textContent = p2Score;
    }

    winner(p2Score, winningScore);
});

resetBtn.addEventListener('click', reset);

inputScore.addEventListener('keypress', (e) => {
    if ('Enter' === e.key) {
        let inputtedWinningScore = Number(e.target.value);
        winningScore = inputtedWinningScore
        winningScoreDisplay.textContent = inputtedWinningScore;
        inputScore.value = '';
        reset();
    }
});

