// Single Condition
const age = 27;

if (age === 27) {
    console.log('Yes, i\'m 27.');
} else {
    console.log('Some stupid lie.');
}

// Multiple Condition
const isAdmin = true,
    profileOwner = false;

if (isAdmin && profileOwner) {
    console.log('Yes, i am admin and i am the owner of the profile.');
} else {
    console.log('Something went wrong.');
}

//Ternary Operator
let isEverythingWrite = (isAdmin && profileOwner)
    ? 'Yes, i am admin and i am the owner of the profile.'
    : 'Something went wrong.'

console.log(isEverythingWrite);

// Conditioning with Logical AND OR
isEverythingWrite = isAdmin && 'Yes, i am admin and i am the owner of the profile.';

console.log(isEverythingWrite);

// if    else if

const price = 15;

if (price <= 10) {
    console.log('No Shipping');
} else if (price <= 20) {
    console.log('Free Shipping available.');
} else {
    console.log('Special discount apply.');
}