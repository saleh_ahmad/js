//Condition
// if(//if we give proper username and password){
//   //Execution we can access our facebook profile
// }
// const age = 20;

// if (age === 18) {
//   console.log("Yea  I am 18");
// } else {
//   console.log("Some stupid lie");
// }

// const isAdmin = true;
// const profileOwner = false;

// if (isAdmin && profileOwner) {
//   console.log("Yes I am admin and I am the owner of the profile");
// } else {
//   console.log("Something went wrong");
// }

//Ternary operator

// const isEverythingRight = (isAdmin && profileOwner) ? "Yes I am admin and I am the owner of the profile" : "Something went wrong";
// console.log(isEverythingRight)

//Alternative way to write condition
// const isEverythingRight =
//   isAdmin && "Yes I am admin and I am the owner of the profile";
// if (!isEverythingRight) {
//   console.log("Something went wrong");
// }
// console.log(isEverythingRight);

//const price = 10;

// if (price <= 10) {
//   //if price less then or equal to $10 NO shipping

//   console.log("No shipping");
// } else if (price <= 20) {
//   //if price greater then or equal to $20 free shipping

//   console.log("Free shipping available");
// } else {
//   console.log("special discount applied");
//   //if price greater then $20 special discount available
// }

//Switch statement
const product = "blah";
switch (product) {
  case "Blue Yeti":
    console.log("Better Microphone");
    break;
  case "Snowball":
    console.log("Good Microphone");
    break;
  case "Rode":
    console.log("Ok");
    break;
  default:
    console.log("Some unknown microphone");
}
