const product = 'Blue Yeti';

switch (product) {
    case 'Blue Yeti':
        console.log('Better Microphone');
        break;
    case 'Snowball':
        console.log('Good Microphone');
        break;
    case 'Rode':
        console.log('Okay');
        break;
    default:
        console.log('Some unknown Microphone');
}