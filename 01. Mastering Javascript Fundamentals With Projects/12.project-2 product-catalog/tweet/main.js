const tweetInput = document.querySelector('#tweet'),
    addBtn = document.querySelector('#submit-btn'),
    filterTweet = document.querySelector('#filter'),
    tweetList = document.querySelector('.tweet-list'),
    msg = document.querySelector('#msg');

let tweetData = [];

function getDataFromLocalStorage() {
    return (localStorage.getItem('tweetItems') === null)
        ? []
        : JSON.parse(localStorage.getItem('tweetItems'));
}

function setDataToLocalStorage(item) {
    const items = (localStorage.getItem('tweetItems') === null)
        ? []
        : JSON.parse(localStorage.getItem('tweetItems'));

    items.push(item);
    localStorage.setItem('tweetItems', JSON.stringify(items));
}

function deleteDataFromLocalStorage(id) {
    const items = JSON.parse(localStorage.getItem('tweetItems'));

    let result = items.filter((product) => {
        return product.id !== id;
    });

    localStorage.setItem('tweetItems', JSON.stringify(result));

    if (result.length === 0) {
        location.reload();
    }
}

tweetData = getDataFromLocalStorage();

//Load All Event Listener
function loadEventListener() {
    //Delete Item
    tweetList.addEventListener('click', deleteItem);

    //Load Data
    window.addEventListener('DOMContentLoaded', getData.bind(null, tweetData))

    //Add Item
    addBtn.addEventListener('click', addItem);

    //Filter Item
    filterTweet.addEventListener('keyup', filterItem);
}

//Handling Message
function showMessage(message = '') {
    msg.innerHTML = message;
}

// Get data from the store & populate UI
function getData(tweetData) {
    if (tweetData.length === 0) {
        showMessage('Please add Tweet.')
        return;
    }

    msg.innerHTML = '';
    let li;
    tweetData.forEach(({id, tweet}) => {
        li = document.createElement('li');
        li.classList.add('list-group-item', 'collection-item')
        li.id = `tweet-${id}`
        li.innerHTML = `<span class="tweet">${tweet}</span> <i class="fas fa-trash-alt float-right dlt-btn"></i>`;

        tweetList.appendChild(li);
    });
}

const addItem = e => {
    e.preventDefault();

    const tweet = tweetInput.value;

    let id = (tweetData.length === 0)
        ? 0
        : tweetData[tweetData.length - 1].id + 1;

    //Validation
    if (tweet === '') {
        alert('Please write tweet.');
        return;
    }

    const data = {
        id,
        tweet
    };

    tweetData.push(data);
    setDataToLocalStorage(data);

    tweetList.innerHTML = '';
    getData(tweetData);

    tweetInput.value = '';
}

//Filter Item from productData
const filterItem = e => {
    const text = e.target.value.toLowerCase();
    let itemFound = false;

    document.querySelectorAll('.tweet-list .collection-item')
        .forEach(item => {
            const tweet = item.firstElementChild.textContent.toLowerCase();

            if (tweet.indexOf(text) === -1) {
                item.style.display = 'none';
            } else {
                item.style.display = 'block';
                itemFound = true;
            }
        });

    itemFound ? showMessage() : showMessage('No Tweet Found.');
};

//Delete Item from productData
const deleteItem= e => {
    if (e.target.classList.contains('dlt-btn')) {
        //Removing target from UI
        const target = e.target.parentElement;
        e.target.parentElement.parentElement.removeChild(target);

        /*
         * Removing Item from store
         * return result array
         */
        const id = parseInt(target.id.split('-')[1]);

        tweetData = tweetData.filter((product) => {
            return product.id !== id;
        });

        deleteDataFromLocalStorage(id);
    }
};

loadEventListener();


