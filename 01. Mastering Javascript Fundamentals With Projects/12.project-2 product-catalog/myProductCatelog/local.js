//Storage

/*
 * Database
 * getting from server/without server
 */

/*
 * Local Storage
 * Data store in local
 * We can only store string
 */

//Setting Item
localStorage.setItem('firstName', 'Saleh');
localStorage.setItem('lastName', 'Ahmad');
localStorage.setItem('age', 20);

//Getting Item
const firstName = localStorage.getItem('firstName');
const age = localStorage.getItem('age');
console.log(firstName);
console.log(typeof age);

const person = {
    firstName : 'Saleh',
    lastName : 'Ahmad',
    age : 20
};

localStorage.setItem('person', JSON.stringify(person));
const getPerson = JSON.parse(localStorage.getItem('person'));
console.log(getPerson);

//Remove Local Storage

//Remove single item
localStorage.removeItem('age');

//Remove all
localStorage.clear();