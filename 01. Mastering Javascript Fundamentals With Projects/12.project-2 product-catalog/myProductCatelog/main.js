const filterInput = document.querySelector('#filter'),
    nameInput = document.querySelector('#product-name'),
    priceInput = document.querySelector('#product-price'),
    addBtn = document.querySelector('#add-product'),
    productList = document.querySelector('.collection'),
    msg = document.querySelector('#msg');

let productData = [];

//Local Storage Functions
function getDataFromLocalStorage() {
    return (localStorage.getItem('productItems') === null)
        ? []
        : JSON.parse(localStorage.getItem('productItems'));
}

function setDataToLocalStorage(item) {
    const items = (localStorage.getItem('productItems') === null)
        ? []
        : JSON.parse(localStorage.getItem('productItems'));

    items.push(item);
    localStorage.setItem('productItems', JSON.stringify(items));
}

function deleteItemFromLocalStorage(id) {
    const items = JSON.parse(localStorage.getItem('productItems'));

    let result = items.filter((product) => {
        return product.id !== id;
    });

    localStorage.setItem('productItems', JSON.stringify(result));

    if (result.length === 0) {
        location.reload();
    }
}

productData = getDataFromLocalStorage();

//Load All Event Listener
function loadEventListener() {
    //Delete Item
    productList.addEventListener('click', deleteItem);

    //Load Data
    window.addEventListener('DOMContentLoaded', getData.bind(null, productData))

    //Add Item
    addBtn.addEventListener('click', addItem);

    //Filter Item
    filterInput.addEventListener('keyup', filterItem);
}

//Handling Message
function showMessage(message = '') {
    msg.innerHTML = message;
}

// Get data from the store & populate UI
function getData(productData) {
    if (productData.length === 0) {
        showMessage('Please add item to your catalog.')
        return;
    }

    msg.innerHTML = '';
    let li;
    productData.forEach(({id, name, price}) => {
        li = document.createElement('li');
        li.classList.add('list-group-item', 'collection-item')
        li.id = `product-${id}`
        li.innerHTML = `<strong>${name}</strong> - $<span class="price">${price}</span> <i class="fas fa-trash-alt float-right dlt-btn"></i>`;

        productList.appendChild(li);
    });
}

//Adding Item to the productData
const addItem = e => {
    e.preventDefault();

    const name = nameInput.value,
        price = priceInput.value;

    let id = (productData.length === 0)
        ? 0
        : productData[productData.length - 1].id + 1;

    //Validation
    if (name === '' || price === '') {
        alert('Please fill up necessary information.');
        return;
    }

    if (!(!isNaN(parseFloat(price)) && isFinite(price))) {
        alert('Please insert a valid price.');
        return;
    }

    const data = {
        id,
        name,
        price
    };
    productData.push(data);
    setDataToLocalStorage(data);

    productList.innerHTML = '';
    getData(productData);

    nameInput.value = priceInput.value = '';
}

//Filter Item from productData
const filterItem = e => {
    const text = e.target.value.toLowerCase();
    let itemFound = false;

    document.querySelectorAll('.collection .collection-item')
        .forEach(item => {
            const productName = item.firstElementChild.textContent.toLowerCase();
            const productPrice = item.children[1].textContent.toLowerCase();

            if ((productName.indexOf(text) === -1) && (productPrice.indexOf(text) === -1)) {
                item.style.display = 'none';
            } else {
                item.style.display = 'block';
                itemFound = true;
            }
        });

    itemFound ? showMessage() : showMessage('No Item Found.');
};

//Delete Item from productData
const deleteItem= e => {
    if (e.target.classList.contains('dlt-btn')) {
        //e.target.remove();
        //e.target.parentElement.remove();

        //Removing target from UI
        const target = e.target.parentElement;
        e.target.parentElement.parentElement.removeChild(target);

        /*
         * Removing Item from store
         * return result array
         */
        const id = parseInt(target.id.split('-')[1]);

        productData = productData.filter((product) => {
            return product.id !== id;
        });

        deleteItemFromLocalStorage(id);
    }
};

loadEventListener();
