//const text = "I won't repeat my code";
//let number = 1;
// while (number < 6) {
//   console.log(`${text} repeated in ${number} times`);
//   number ++;
// }

// for(let number = 1; number < 6; number++){
//   console.log(`${text} repeated in ${number} times`);
// }

//const str = 'You are a Hero';
// console.log(str.length)

// for (let number = 0; number < str.length; number++) {
//   console.log(` ${str[number]}`);
// }

//const profile = ["samim", "Hasan", 25, "Web programmer"];
// let i = 0;
// while (i < profile.length) {
//   if(profile[i] === 25 ){
//     //console.log('I don\'t want to show my age')
//     i++
//     continue;
//   }else{
//     console.log(profile[i]);
//   }

//   i++;
// }

// for (let i = 0; i < profile.length; i++) {
//   console.log(profile[i]);
// }

//Array looping
//For of loop
// const profile = ["samim", "Hasan", 25, "Web programmer"];
// for(let value of profile){
//   console.log(value)
// }

//Object looping
//For in loop
// const profile = {
//   firstName: "samim",
//   lastName: "Fazlu",
//   age: 25,
//   profession: "Web developer"
// };
// //console.log(profile['firstName'])

// for (let key in profile) {
//   console.log(profile[key]);
// }
