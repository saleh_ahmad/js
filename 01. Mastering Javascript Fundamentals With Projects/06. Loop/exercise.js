// Exercise 1
let num = 1;

while(num <= 10) {
    console.log(num);
    num += 2;
}
console.log('Output will be 1,3,5,7 and 9 as in every iteration, we add 2 with num.');

// Exercise 2
num = 1

while(num <= 20) {
    if(num % 4 === 0){
        console.log(num);
    }
    num++;
}

console.log('Output will be 4,8,12,16 and 20. Output will show when we divide the number by 4 and the remainder will be 0');

// Exercise 3
/*num = 100;
while(num < 150) {
    console.log(num + 1);
    num--;
}*/

console.log('Infinite Loop. num will always less than 150 as we always decrement 1 from num.');

//Exercise 4
/*
 * It can be done by using two loops in other languages but don't know how to skip break after console log in JS.
 * That's why doing it in this way.
 */
let i = 1;

do {
    switch (i) {
        case 1:
            console.log('$');
            break;
        case 2:
            console.log('$$');
            break;
        case 3:
            console.log('$$$');
            break;
        case 4:
            console.log('$$$$');
            break;
        case 5:
            console.log('$$$$$');
            break;
        case 6:
            console.log('$$$$$$');
    }
    ++i;
} while (i <= 6);

//Exercise 5
/*
 * Write a program that prints the numbers from 1 to 100.
 * But for multiples of three print “Fizz” instead of the number and for the multiples of five print “Buzz”.
 * For numbers which are multiples of both three and five print “FizzBuzz”.
 *
 * 1
 * 2
 * Fizz
 * 4
 * Buzz
 * Fizz
 * 7
 * 8
 * Fizz
 * Buzz
 * 11
 * Fizz
 * 13
 * 14
 * FizzBuzz
 */
i = 0;

do {
    ++i;
    if ((i % 3 === 0) && (i % 5 === 0)) {
        console.log('FizzBUZZ');
        continue;
    }else if (i % 3 === 0) {
        console.log('Fizz');
        continue;
    } else if (i % 5 === 0) {
        console.log('Buzz');
        continue;
    }
    console.log(i);
} while (i < 100);