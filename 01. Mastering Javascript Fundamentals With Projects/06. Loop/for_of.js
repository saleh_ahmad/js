// Array Looping
const profile = ['Saleh', 'Ahmad', 27, 'Web Programmer'];

for (value of profile) {
    console.log(value);
}
