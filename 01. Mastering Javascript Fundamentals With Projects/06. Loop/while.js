let count = 1;

while (count < 6) {
    console.log('Count is: ' + count);
    ++count;
}

const text = 'I won\'t repeat my code.';
let number = 1;

while (number < 6) {
    console.log(text);
    console.log(`${text} - My code repeat in ${number} times.`);
    ++number;
}


const profile = ['Saleh', 'Ahmad', 27, 'Web Programmer'];
let i = 0;

while (i < profile.length) {
    console.log(profile[i]);
    ++i;
}