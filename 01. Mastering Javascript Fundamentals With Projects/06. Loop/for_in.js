// Object Looping
const profile = {
    name : 'Saleh Ahmad',
    age : 27,
    profession : 'Web Developer',
    hobby : 'javascript'
};

console.log(profile['name']);
console.log(profile.name);

for (let key in profile) {
    console.log(profile[key]);
}

const surName = 'name';
//Dynamically access value
console.log(profile[surName]);
