let i = 1;

do {
    console.log(`Iteration: ${i}`);
    ++i;
} while (i < 10);

i = 1;
do {
    console.log(`Iteration: ${i}`);
    ++i;
} while (i > 2);