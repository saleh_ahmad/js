// Example 1
const str = 'Hello';

for (let i = 0; i < str.length; ++i) {
    console.log(str[i]);
}

// Example 2
const text = 'I won\'t repeat my code.';

for (let j = 1; j < 6; ++j) {
    console.log(`${text} - My code repeat in ${j} times.`);
    ++j;
}

// Example 3
const profile = ['Saleh', 'Ahmad', 27, 'Web Programmer'];

for (let k = 0; k < profile.length; ++k) {
    console.log(profile[k]);
}

// Example 4
const str2 = 'You are a Hero';

for (i = 0; i < str2.length; ++i) {
    console.log(str2[i]);
}