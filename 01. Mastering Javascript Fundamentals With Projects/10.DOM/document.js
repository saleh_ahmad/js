console.log(document);
console.dir(document);

//head
let val = document.head;
console.log(val);

//body
val = document.bodu;
console.log(val);

//All Elements
val = document.all;
console.log(val);

val = document.all[5];
console.log(val);

val = document.title;
console.log(val);

//Links
val = document.links;
console.log(val);

val = document.links[0];
console.log(val);

//Character Set
val = document.characterSet;
console.log(val);

//Forms
val = document.forms;
console.log(val);

val = document.forms[0];
console.log(val);

//Redirect to a blank Page
function redirect_blank(url) {
    const a = document.createElement('a');
    a.target = "_blank";
    a.href = url;
    a.click();
}