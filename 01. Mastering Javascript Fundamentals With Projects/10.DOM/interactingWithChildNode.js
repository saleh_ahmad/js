let val = document.querySelector('.product-collection');

console.log(val);

console.log(val.childNodes);
console.log(val.childNodes[0]);

/*
 * 1 - Element
 * 2 - Attribute (deprecated)
 * 3 - Text node
 * 8 - Comment
 * 9 - Document itself
 * 10 - Doctype
 */
console.log(val.childNodes[0].nodeName);
console.log(val.childNodes[0].nodeType);
console.log(val.childNodes[1].nodeName);
console.log(val.childNodes[1].nodeType);

if (val.childNodes[1].nodeType !== 3) {
    val.childNodes[1].classList.add('myClass');
}