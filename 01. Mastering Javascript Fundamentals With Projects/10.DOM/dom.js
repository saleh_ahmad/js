//All about document object
//=================================
let val;
//val = document;
// val = document.all;
// val = document.all[5];
// val = document.head;
// val = document.title;
// val = document.body;
// val = document.body.className;
// val = document.body.id;
// val = document.links[0];
// val = document.links[0].getAttribute("href");
// val = document.characterSet;
// val = document.forms[0];
// val = document.forms[0].action;
// val = document.forms[0].method;
// val = document.scripts;
// console.log(val);

//Accessing Specific element (better than
//=======================================
// previous way May have some kind of speed issue)
//https://code.tutsplus.com/tutorials/the-30-css-selectors-you-must-memorize--net-16048
// val = document.getElementById("container");
// val = document.getElementsByClassName("container")[0];
// val = document.getElementsByTagName("body")[0];
// val = document.querySelector("#container");
// val = document.querySelector(".product-collection-item");
// val = document.querySelectorAll(".product-collection-item")[1];

//HTMLCollection You can't  access all array specific method .if you want to please try Array.form()

// val = document.getElementsByClassName("product-collection-item");
// val = Array(val);
// val.forEach(node => console.log(node));
// for (let i = 0; i < val.length; i++) {
//   console.log(val[i]);
// }
// for (let node of val) {
//   console.log(node);
// }

//NodeList -You can access all array method
//===========================================
// val = document.querySelectorAll(".product-collection-item");
// console.log(val);
// // val = Array(val);
// val.forEach(node => console.log(node));
// for (let i = 0; i < val.length; i++) {
//   console.log(val[i]);
// }
// for (let node of val) {
//   console.log(node);
// }
// console.log(val);

//manipulating element
//========================================

//selection
// val = document.querySelector(".product-collection-item");

//manipulation
// val.style.color = "red";
// val.style.backgroundColor = "Green";
// val.textContent = "Microphone";
// val.innerText = "Shirt";
// val.innerHTML = "<em>Shirt</em>";
// val = document.querySelector("a");
// val = document.querySelector("a").getAttribute("href");
// val = document.querySelector("a").setAttribute("href", "https://facebook.com");

// val = document.querySelector(".product-collection-item");
//val = val.classList;
// val.className = "MyClass";
// val.classList.add("MyClass");

// val = document.querySelector(".product-collection");
// console.log(val);

// val =
//   val.children[0].nextElementSibling.nextElementSibling.previousElementSibling.parentElement
//     .parentElement;
// 1 - Element
// 2 - Attribute (deprecated)
// 3 - Text node
// 8 - Comment
// 9 - Document itself
// 10 - Doctype
// val = val.childNodes[1].nodeName;
// if (val.childNodes[1].nodeType !== 3) {
//   val.childNodes[1].classList.add("MyClass");
// }
// <li class="product-collection-item">sneakers</li>;
// const ul = document.querySelector("ul");
// const li = document.createElement("li");
// li.className = "product-collection-item";
// li.appendChild(document.createTextNode("MyItem"));
// ul.prepend(li);
// const oldHeading = document.querySelector(".h1");
// const newHeading = document.createElement("h1");
// newHeading.appendChild(document.createTextNode("My Updated Product List"));
// const container = document.querySelector(".container");
// container.replaceChild(newHeading, oldHeading);
// oldHeading.replaceWith(newHeading);

// const list = document.querySelector("ul.product-collection");
// list.lastElementChild.remove();

//list.removeChild(list.lastElementChild);
//You can also access by following way
//selecting element
// const li = document.querySelector("li:nth-child(5)");
// li.remove()
//OR this way
// const li = document.querySelector("li:last-child");
// li.remove();

// console.log(ul);
// console.log(list);

//DOM EVENTS
//===============================
// https://developer.mozilla.org/en-US/docs/Web/Events

// const h1 = document.querySelector("h1");
// const li = document.querySelector("li");
// const lis = document.querySelectorAll("li");
// const ul = document.querySelector("ul");
// const form = document.forms[0];
// const input = document.getElementById("productName");
// function evtInformation(evt) {
//   evt.preventDefault();
//   console.log("Type:", evt.type);
//   console.log("Target", evt.target);
//   const li = document.createElement("li");
//   li.textContent = input.value;
//   li.className = "product-collection-item";
//   ul.appendChild(li);
// console.log("Target value", evt.target.innerText);
// console.log("Offset-x", evt.offsetX);
// console.log("offset-y", evt.offsetY);
// console.log("Client-x", evt.clientX);
// console.log("Cline-y", evt.clientY);
// console.log("Clicked H1");
// }
// h1.addEventListener("click", evtInformation);
// h1.addEventListener("dblclick", evtInformation);

// ul.addEventListener("click", evt => {
//   if ((evt.target.className = "product-collection-item")) {
//     console.log(evt.target);
//   }
// });
// lis.forEach(li => {
//   li.addEventListener("click", evtInformation);
// });
// form.addEventListener("submit", evtInformation);

//Event bubbling
// const li = document.querySelector("li");
// const ul = document.querySelector("ul");
// const container = document.querySelector(".container");
// li.addEventListener("click", () => {
//   console.log("You clicked li");
// });
// ul.addEventListener("click", () => {
//   console.log("You clicked ul");
// });
// container.addEventListener("click", evt => {
//   if (evt.target.id === "sample") {
//     evt.target.classList.add("custom");
//   }
// });

//Event delegation
