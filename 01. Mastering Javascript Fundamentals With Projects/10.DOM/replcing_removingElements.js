const oldHeading = document.querySelector('.h1');

const newHeading = document.createElement('h1');

newHeading.appendChild(document.createTextNode('My Updated Product List'));

const container = document.querySelector('.container');

//Way 1
//container.replaceChild(newHeading, oldHeading)

//Way 2
oldHeading.replaceWith(newHeading);


const list = document.querySelector('.product-collection');

//Remove Last Child
list.lastElementChild.remove();
console.log(list);
list.removeChild(list.lastElementChild);
console.log(list);