let querySelector = document.querySelector('.product-collection-item');
console.log(querySelector);

querySelector.style.color = 'red';

//camelCase
querySelector.style.backgroundColor = 'green';

//Change Text
querySelector.textContent = 'Microphone';
querySelector.innerText = 'Shirt';

//Change HTML
querySelector.innerHTML = '<em>Shirt</em>';


querySelector = document.querySelector('a').getAttribute('href');
console.log(querySelector);

//Change Value
querySelector = document.querySelector('a').setAttribute('href', 'https://facebook.com');
console.log(querySelector);


querySelector = document.querySelector('.product-collection-item');

//Fetch Classname
console.log(querySelector.className);

//Fetch Id
console.log(querySelector.id);

//Change Classname
/*querySelector.className = 'myClass';
console.log(querySelector.className);*/

//Fetch Classlist
console.log(querySelector.classList);

//Add a new class
querySelector.classList.add('myClass');
console.log(querySelector.classList);