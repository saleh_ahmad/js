/*
 * Top Most Object
 */
const time = 3000;

if (time === 5000) {
    setTimeout(() => {
        //Open a new window
        open('http://google.com', '_blank');
    }, 5000)
} else {
    setTimeout(() => {
        //Redirect URL in the same page
        location.href = 'https://www.google.com';
    }, 3000)
}
