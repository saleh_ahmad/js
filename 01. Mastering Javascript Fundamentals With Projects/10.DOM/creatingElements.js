const ul = document.querySelector('ul');

const li1 = document.createElement('li');
li1.className = 'product-collection-item';
li1.appendChild(document.createTextNode('Append My Item'));

const li2 = document.createElement('li');
li2.className = 'product-collection-item';
li2.appendChild(document.createTextNode('Prepend My Item'));

ul.appendChild(li1);
ul.prepend(li2);

console.log(ul);