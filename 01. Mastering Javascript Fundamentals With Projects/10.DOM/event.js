//https://developer.mozilla.org/en-US/docs/Web/Events

const button = document.querySelector('button');

button.addEventListener('click', () => {
    console.log('Button Clicked');
});


const evtInfo = (e) => {
    console.log(e);
    console.log('Type: ', e.type);
    console.log('Target: ',e.target);
    console.log('Offset X: ',e.offsetX);
    console.log('Offset Y: ',e.offsetY);
    console.log('Client X: ',e.clientX);
    console.log('Client Y: ',e.clientY);

    console.log('Clicked Hi');
}

const formEvtInfo = (e) => {
    e.preventDefault();
    console.log('Type: ', e.type);

    //Get Input Value
    const input = document.getElementById('productName');
    console.log('Input Value: ', input.value);
}

const h1 = document.querySelector('h1');
/*
 * Anonymous Function
 * Event Listener
 */
h1.addEventListener('click', evtInfo);

const li = document.querySelector('li');
li.addEventListener('click', evtInfo);

const allLi = document.querySelectorAll('li');
allLi.forEach((li) => {
    li.addEventListener('click', evtInfo);
});


const form = document.forms[0];
form.addEventListener('submit', formEvtInfo);