//Event Bubbling
/*const li = document.querySelector('li');
li.addEventListener('click', () => {
    console.log('You clicked li');
});

const ul = document.querySelector('ul');
ul.addEventListener('click', () => {
    console.log('You clicked ul');
});

const container = document.querySelector('.container');
container.addEventListener('click', () => {
    console.log('You clicked Container');
});

const body = document.querySelector('body');
body.addEventListener('click', () => {
    console.log('You clicked body');
});*/

//Event Delegation
const container2 = document.querySelector('.container');
container2.addEventListener('click', (e) => {
    if (e.target.id === 'sample') {
        console.log(e.target);
        e.target.classList.add('custom');
    }
});

//Example
const ul = document.querySelector('ul');

const evtInfo = (e) => {
    console.log(e);
    console.log('Type: ', e.type);
    console.log('Target: ',e.target);
    console.log('Offset X: ',e.offsetX);
    console.log('Offset Y: ',e.offsetY);
    console.log('Client X: ',e.clientX);
    console.log('Client Y: ',e.clientY);

    console.log('Clicked Hi');
}

const formEvtInfo = (e) => {
    e.preventDefault();
    console.log('Type: ', e.type);

    //Get Input Value
    const input = document.getElementById('productName');
    console.log('Input Value: ', input.value);
    const li = document.createElement('li');
    li.textContent = input.value;
    li.className = 'product-collection-item';

    ul.appendChild(li);
}

const allLi = document.querySelectorAll('li');

ul.addEventListener('click', (e) => {
    if (e.target.className === 'product-collection-item') {
        console.log(e.target);
    }
});

/*allLi.forEach((li) => {
    li.addEventListener('click', evtInfo);
});*/

const form = document.forms[0];
form.addEventListener('submit', formEvtInfo);