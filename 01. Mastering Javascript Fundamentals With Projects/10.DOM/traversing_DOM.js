let val = document.querySelector('.product-collection');

console.log(val);

console.log(val.childNodes);

//Better
console.log(val.children);
console.log(val.children.length);
console.log(val.children[0]);

console.log(val.children[0].nextSibling);

//Better
console.log(val.children[0].nextElementSibling);

//Parent
console.log(val.children[0].parentNode);

//Better
console.log(val.children[0].parentElement);
console.log(val.children[0].parentElement.parentElement);