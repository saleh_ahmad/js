//Exercise 1
const body = document.querySelector('body');

const container = document.createElement('div');
container.className = 'container';
container.id = 'container';

const h1 = document.createElement('h1');
h1.className = 'h1';
h1.textContent = 'Product List';
container.appendChild(h1);

const ul = document.createElement('ul');
ul.className = 'product-collection';
ul.classList.add('mb-3');

const li = document.createElement('li');
li.className = 'product-collection-item';
li.id = 'sample';
li.textContent = 'Shoes';
ul.appendChild(li);

container.appendChild(ul);

body.prepend(container);

//Exercise 2
//Uppercase every character
const name = document.querySelector('#name');
name.addEventListener('keyup', function(e) {
    this.value = this.value.toUpperCase();
});

//Only Integer will be allowed
const age = document.querySelector('#age');

age.addEventListener('keypress', function(e) {
    if ((e.keyCode < 48) || (e.keyCode >= 57)) {
        e.preventDefault();
    }
});

//Disable Right Click
document.addEventListener('contextmenu', e => e.preventDefault());

//Reverse String
const dblClickBtn = document.querySelector('#dblClickBtn');
dblClickBtn.addEventListener('dblclick', function(e) {
    this.textContent = this.textContent.split('').reverse().join('');
});

//Add Item
const inputtedItem = document.querySelector('#listItem');
const addBtn = document.querySelector('#listItemBtn');
addBtn.addEventListener('click', () => {
    if (inputtedItem.value) {
        const newLi = document.createElement('li');
        newLi.className = 'product-collection-item';
        newLi.textContent = inputtedItem.value;
        ul.appendChild(newLi);
        inputtedItem.value = '';
    }
});

//Change Color
const colorChangingText = document.querySelector('#colorChangingText');
colorChangingText.addEventListener('mouseover', (e) => {
    e.target.className = 'custom';
});

colorChangingText.addEventListener('mouseout', (e) => {
    e.target.classList.remove('custom');
});

//selectText
/*
 * The event is not available for all elements in all languages. For example, in HTML,
 * select events can be dispatched only on form <input type="text"> and <textarea> elements.
 */
function logSelection(e) {
    const log = document.getElementById('log');
    const selection = e.target.value.substring(e.target.selectionStart, e.target.selectionEnd);
    log.textContent = `You selected: ${selection}`;
}

const input = document.querySelector('#selectText');
input.addEventListener('select', logSelection);

//Exercise 3
//Double click to delete item
ul.addEventListener('dblclick', (e) => {
    if (e.target.className === 'product-collection-item') {
        ul.removeChild(e.target);
    }
});

//Exercise 4
const someTextElement = document.querySelector('#someTextElement');

const beforebeginElement = document.querySelector('#beforebeginElement');
beforebeginElement.addEventListener('click', () => {
    let tempDiv = document.createElement('p');
    tempDiv.className = 'text-monospace';
    tempDiv.textContent = 'This is before p tag.';

    someTextElement.insertAdjacentElement('beforebegin', tempDiv);
});

const afterbeginElement = document.querySelector('#afterbeginElement');
afterbeginElement.addEventListener('click', () => {
    let tempDiv = document.createElement('p');
    tempDiv.className = 'text-monospace';
    tempDiv.textContent = 'This is inside p tag before text.';

    someTextElement.insertAdjacentElement('afterbegin', tempDiv);
});

const beforeendElement = document.querySelector('#beforeendElement');
beforeendElement.addEventListener('click', () => {
    let tempDiv = document.createElement('p');
    tempDiv.className = 'text-monospace';
    tempDiv.textContent = 'This is inside p tag after text.';

    someTextElement.insertAdjacentElement('beforeend', tempDiv);
});

const afterendElement = document.querySelector('#afterendElement');
afterendElement.addEventListener('click', () => {
    let tempDiv = document.createElement('p');
    tempDiv.className = 'text-monospace';
    tempDiv.textContent = 'This is after p tag.';

    someTextElement.insertAdjacentElement('afterend', tempDiv);
});

//=======================================================================

const someText = document.querySelector('#someText');

const beforebegin = document.querySelector('#beforebegin');
beforebegin.addEventListener('click', () => {
    someText.insertAdjacentHTML('beforebegin', '<p class="text-monospace">This is before p tag</p>');
});

const afterbegin = document.querySelector('#afterbegin');
afterbegin.addEventListener('click', () => {
    someText.insertAdjacentHTML('afterbegin', '<p class="text-monospace">This is inside p tag before text</p>');
});

const beforeend = document.querySelector('#beforeend');
beforeend.addEventListener('click', () => {
    someText.insertAdjacentHTML('beforeend', '<p class="text-monospace">This is inside p tag after text</p>');
});

const afterend = document.querySelector('#afterend');
afterend.addEventListener('click', () => {
    someText.insertAdjacentHTML('afterend', '<p class="text-monospace">This is after p tag</p>');
});

//=========================================================================

const unorderedList = document.querySelector('#unordered-list');
const old = document.querySelector('#old');

const addNode = document.querySelector('#addNode');
addNode.addEventListener('click', (e) => {
    const newNode = document.createElement('li');
    newNode.className = 'item';
    newNode.textContent = 'New Item Before Node.';

    unorderedList.insertBefore(newNode, old);
});

const insertAfter = document.querySelector('#insertAfter');
insertAfter.addEventListener('click', (e) => {
    const newNode = document.createElement('li');
    newNode.className = 'item';
    newNode.textContent = 'New Item After Old Node.';

    old.after(newNode);
});
