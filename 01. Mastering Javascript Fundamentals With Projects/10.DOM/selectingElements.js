console.log(document.getElementById('container'));

console.log(document.getElementsByClassName('container'));
console.log(document.getElementsByClassName('container')[0]);

console.log(document.getElementsByTagName('body'));
console.log(document.getElementsByTagName('body')[0]);

//Show only 1
console.log(document.querySelector('.product-collection-item'));
console.log(document.querySelector('#container'));

//Show All
console.log(document.querySelectorAll('.product-collection-item'));
console.log(document.querySelectorAll('.product-collection-item')[1]);
