let htmlCollection = document.getElementsByClassName('product-collection-item');

//This will not work
//htmlCollection.forEach(node => console.log(node));

//it will work
for (let i = 0; i < htmlCollection.length; ++i) {
    console.log(htmlCollection[i]);
}

//it will work
for (let value of htmlCollection) {
    console.log(value);
}

/*
 * Convert it into an array
 * Now it will work
 */
htmlCollection = Array(htmlCollection);
htmlCollection.forEach(node => console.log(node));

const querySelector = document.querySelectorAll('.product-collection-item');
querySelector.forEach(node => console.log(node));