/*
 * First Exercise
 */
/*const profile = {};

profile.name = 'Saleh';
profile.age = 27;
profile.profession = 'Web Developer';
profile.hobby = 'javascript';*/

const profile = {
    name : 'Saleh',
    age : 27,
    profession : 'Web Developer',
    hobby : 'javascript'
};

const bio = 'I am ' + profile.name + ' and ' + profile.age
    + '. I\'m a ' + profile.profession
    + '. I love "' + profile.hobby + '".';

console.log(bio);


/*
 * Second Exercise
 */
const someObj = {};

someObj._name = 'Hedwig';

console.log('It is correct. _ is allowed as a starting character in variable name.');

someObj.age = 6;
console.log('It is correct. It is allowed to assign any type of value in object.');

const prop = 'color';
someObj[prop] = 'red';
console.log('It is correct. prop was defined as a variable.');

someObj.123 = true;
console.log('It is incorrect. number is not allowed as a starting character in variable name. I can write the invalid syntex by following way.');
someObj['123'] = true;

/*
 * Third Exercise
 */
const movie = [
    {
        id : 1,
        name : 'movie1',
        rating : 5,
        category : 'demo'
    },
    {
        id : 2,
        name : 'movie2',
        rating : 3.5,
        category : 'demo'
    },
    {
        id : 3,
        name : 'movie3',
        rating : 4.5,
        category : 'demo'
    },
    {
        id : 4,
        name : 'movie4',
        rating : 4.9,
        category : 'demo2'
    },
    {
        id : 5,
        name : 'movie5',
        rating : 4.1,
        category : 'demo'
    },
    {
        id : 6,
        name : 'movie6',
        rating : 4.7,
        category : 'demo2'
    },
    {
        id : 7,
        name : 'movie7',
        rating : 4.8,
        category : 'demo2'
    },
    {
        id : 8,
        name : 'movie8',
        rating : 4.7,
        category : 'demo3'
    },
    {
        id : 9,
        name : 'movie9',
        rating : 3.9,
        category : 'demo3'
    },
    {
        id : 10,
        name : 'movie10',
        rating : 5,
        category : 'demo'
    },
];

const movieIndex = movie[0];
const string = movieIndex.name + ' got ' + movieIndex.rating + ' rating and under ' + movieIndex.category + ' category';
console.log(string);
