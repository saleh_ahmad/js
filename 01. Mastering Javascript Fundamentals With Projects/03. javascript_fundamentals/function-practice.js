function profileBuilder(fname, lname, age, job, learnInFuture) {
    console.log('My name is ' + fname + ' ' + lname + '. I am ' + age + '. I work as a ' + job + '. I want to learn ' + learnInFuture + '.');
}

profileBuilder('Saleh', 'Ahmad', 27, 'Web Developer', 'Machine Learning');

// Template String
function profileBuilderWIthTemplateString(fname, lname, age, job, learnInFuture) {
    console.log(`My name is ${fname} ${lname}. I am ${age}. I work as a ${job}. I want to learn ${learnInFuture}.`);
}

profileBuilderWIthTemplateString('Saleh', 'Ahmad', 27, 'Web Developer', 'Machine Learning');