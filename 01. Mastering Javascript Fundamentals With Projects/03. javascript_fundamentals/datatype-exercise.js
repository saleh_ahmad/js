const word = 'Karim is a bad boy',
    word2 = "karim is a \"bad\" boy.";

console.log(word);
console.log(word2);

console.log(typeof 43);
console.log(typeof '43');
console.log("43 is a number type but when i put single quote it, it will be a string.");

const line1 = 'I am Saleh Ahmad.',
    line2 = ' I am 25.',
    line3 = ' I\'m a Web Developer.',
    line4 = ' I love "Javascript".';

const line = line1 + line2 + line3 + line4;

console.log(line);