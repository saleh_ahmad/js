function myFunc(name, age, profession, hobby) {
    return `I am ${name}. I\'m ${age}. I\'m a ${profession}. I love "${hobby}".`;
}

const bio = myFunc('Saleh', 27, 'Web Developer', 'Javascript');
console.log(bio);

function movieOutput(movies) {
    const movieIndex = movies[0];

    return `${movieIndex.name} got ${movieIndex.rating} rating and under ${movieIndex.category} category.`;
}

const movies = [
    {
        id : 1,
        name : 'Avengers: Infinity War',
        rating : 5,
        category : 'demo'
    },
    {
        id : 2,
        name : 'movie2',
        rating : 3.5,
        category : 'demo'
    },
    {
        id : 3,
        name : 'movie3',
        rating : 4.5,
        category : 'demo'
    },
    {
        id : 4,
        name : 'movie4',
        rating : 4.9,
        category : 'demo2'
    },
    {
        id : 5,
        name : 'movie5',
        rating : 4.1,
        category : 'demo'
    },
    {
        id : 6,
        name : 'movie6',
        rating : 4.7,
        category : 'demo2'
    },
    {
        id : 7,
        name : 'movie7',
        rating : 4.8,
        category : 'demo2'
    },
    {
        id : 8,
        name : 'movie8',
        rating : 4.7,
        category : 'demo3'
    },
    {
        id : 9,
        name : 'movie9',
        rating : 3.9,
        category : 'demo3'
    },
    {
        id : 10,
        name : 'movie10',
        rating : 5,
        category : 'demo'
    },
];

const singleMovie = movieOutput(movies);
console.log(singleMovie);