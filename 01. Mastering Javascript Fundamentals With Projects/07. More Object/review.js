const product = {
    name : 'T-Shirt',
    price : 250,
    'is available' : true
}

const availability = 'is available';
console.log(product.name);
console.log(product.price);
console.log(product['is available']);
console.log(product[availability]);

for (let i in product) {
    console.log(product[i]);
}
