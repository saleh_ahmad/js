// Random Value
console.log(Math.random());

// Floor Value
console.log(Math.floor(4.4));
console.log(Math.ceil(4.7));
console.log(Math.abs(-3));
console.log(Math.round(4.453434534));

// Generate randomly 0-3 (not generating 3)
console.log(Math.random() * 3);

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min; // The maximum is exclusive and the minimum is inclusive
}

console.log(getRandomInt(1, 10));
