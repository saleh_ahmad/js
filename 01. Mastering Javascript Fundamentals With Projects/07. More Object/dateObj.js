let date = new Date();
console.log(date);

date = new Date(10000);
console.log(date);

date = new Date('December 17, 1995 03:24:00');
console.log(date);

date = new Date(2018, 10, 1, 11, 34, 45);
console.log(date);

date.setFullYear('2010');
console.log(date);

date.setMonth('1')
console.log(date);