/*
 * Copy By Value (primitive (string/number/null/undefined/boolean/symbol))
 * Copy By Reference (reference/complex (object/array/function))
 */
//Copy By Value
let a = 10;
const b = a;

console.log(a);
console.log(b);

a = 20;
console.log(a);
console.log(b);

//Copy By Reference
let obj1 = {value : 3};
const obj2 = obj1;

console.log(obj1);
console.log(obj2);

//obj1 = {value : 5};
obj1.value = 5;
console.log(obj1);
console.log(obj2);
