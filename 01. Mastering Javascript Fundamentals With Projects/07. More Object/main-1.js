//Object key:value  property:value

// const firstName //camel casing
// const FirstName //pascal Notation

// //1.this inside method  = object
// //2.this in a constructor function - some kind of Object
// //3.otherwise -always indicate window
//WAnt to know more about this-https://www.youtube.com/playlist?list=PLyrs5AgsUPcXT6L8r7WdMfYGPYRBA08_X

//Function statement and this binding

// function sayHi(age, profession) {
//   // console.log(this);
//   return `Hi ${this} You are ${age} .You are a ${profession}  `;
// }
// const sayHiWithBinding = sayHi.bind("samim");
// const outputCall = sayHiWithBinding.call(undefined, 25, "web developer");
// const outputApply = sayHiWithBinding.apply(undefined, [25, "web developer"]);
// console.log(outputCall);
// console.log(outputApply);

//function expression and this binding

// const sayHi = function(age, profession) {
//   return `Hi ${this} You are ${age} .You are a ${profession}  `;
// }.bind("samim");
// const outputCall = sayHi.call(undefined, 25, "web developer");
// const outputApply = sayHi.apply(undefined, [25, "web developer"]);
// console.log(outputCall);
// console.log(outputApply);

// //Constructor Function
// function Product(name, price) {
//   this.name = name;
//   this.price = price;
//   this.productDesc = function() {
//     return `${this.name}-$${this.price}`;
//   };
//   console.log(this);
// }

// const product1 = new Product("Sneaker", 60);
// console.log(product1);
// console.log(product1.name);
// console.log(product1.productDesc());

// //Factory Function
// function product(name, price, availability) {
//   return {
//     name,
//     price,
//     "is available": availability,
//     productDesc() {
//       return `${this.name}-$${this.price}`;
//     }
//   };
// }
// console.log(product("Print T-shirt", 10, true));
// console.log(product("Sneaker", 60, true));

//simple way of creating object and accessing key
// const product1 = {
//   name: "print T shirt",
//   price: 10,
//   "is available": true,
//   productDesc() {
//     return `${this.name}-$${this.price}`;
//   }
// };
// const product2 = {
//   name: "Sneaker",
//   price: 60,
//   "is available": true,
//   productDesc() {
//     return `${this.name}-$${this.price}`;
//   }
// };
// console.log(product1.name);
// console.log(product1.price);
// console.log(product1.productDesc());
// console.log(product2.name);
// console.log(product2.price);
// console.log(product2.productDesc());

//Dynamically access properties
// const availability = 'is available'
// console.log(product.name)
// console.log(product['name'])
// console.log(product['is available'])
// console.log(product[availability]);
