// name and price - same key & value - no need to write twice
function product(name, price, availability) {
    return {
        name,
        price,
        'is available': availability,
        productDesc() {
            return `${this.name} - $${this.price}`;
        }
    };
}

console.log(product('Sneaker', 250, true));
console.log(product('T-Shirt', 100, true));
