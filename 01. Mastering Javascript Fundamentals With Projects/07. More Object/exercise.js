//Exercise 1
function max(a, b) {
    return (a > b) ? a : b;
}

console.log(max(10, 15));

//Exercise 2
function showNumber(num) {
    const arr = [];
    let i = num - 1;

    do {
        arr.push(i);
        i--;
    } while(i >= 0);

    return arr;
}

console.log(showNumber(10));

//Exercise 3
function sum(num) {
    let i = num - 1,
        count = 0;

    do {
        count += i;
        i--;
    } while (i >= 0);

    return count;
}

console.log(sum(10));

//Exercise 4
function calculateAverage(arr) {
    let average = 0,
        grade = 'F';

    if (arr.length) {
        let sum = 0;
        for (let i = 0; i < arr.length; ++i) {
            sum += arr[i];
        }

        average = sum/arr.length;

        if (average < 60) {
            grade = 'F';
        } else if (average < 69) {
            grade = 'D';
        } else if (average < 79) {
            grade = 'C';
        } else if (average < 89) {
            grade = 'B';
        } else {
            grade = 'A';
        }
    }

    return grade;
}

console.log(calculateAverage([60, 70, 90, 50, 55]));

//Exercise 5
const movie = {
    title : 'a',
    releaseYear : 2018,
    rating : 4.5,
    director : 'b'
};

function showProperties(movieData) {
    const stringProperties = {};

    for (let key in movieData) {
        if (typeof(movieData[key]) === 'string') {
            stringProperties[key] = movieData[key];
        }
    }

    return stringProperties;
}

console.log(showProperties(movie));
