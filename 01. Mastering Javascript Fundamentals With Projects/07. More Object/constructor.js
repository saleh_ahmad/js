const firstName = 'Saleh'; // camel case
const FirstName = 'Saleh'; // Pascel Notation

function Product(name, price) {
    this.name = name;
    this.price = price;
    this.productDesc = function() {
        return `${this.name} - $${this.price}`;
    }
}

const product1 = new Product('Sneaker', 250);
console.log(product1);