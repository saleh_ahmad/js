//Way 1 (for in)
const product1 = {
    name : 'T-Shirt',
    price : 20,
    availability : true
};

const product2 = {};
for (let key in product1) {
    product2[key] = product1[key];
}

console.log(product2);

//Way 2 (assign)
const product3 = Object.assign({color : 'red'}, product1);
console.log(product3);

//Way 3 (Spread Operator)
const product4 = {...product1, color : 'red'};
console.log(product4);
