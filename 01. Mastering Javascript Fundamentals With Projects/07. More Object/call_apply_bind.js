//Function Statement
function sayHi(age, profession) {
    return `Hi ${this}. You are ${age} and you are a ${profession}.`;
}

sayHi();
sayHi.call();

//======================= call ==========================
// Argument can be any type of variable
sayHi.call('Saleh');

// this, param1, param2
const outputCall = sayHi.call('Saleh', 27, 'Web Developer');
console.log(outputCall);

//======================= apply ==========================
const outputApply = sayHi.apply('Saleh', [27, 'Web Developer']);
console.log(outputApply);

//====================== bind this ======================
const sayHiAfterBinding = sayHi.bind('Saleh');

/*
 * this won't change as we bind the this value by 'bind'
 */
const outputCall2 = sayHiAfterBinding.call('Ahmad', 27, 'Web Developer');
console.log(outputCall2);

const outputApply2 = sayHiAfterBinding.apply('Oyon', [27, 'Web Developer']);
console.log(outputApply2);

//========================================================
function sayHello() {
    console.log('Hello');
}
const anotherFunc = sayHi;
console.log(sayHi);
console.log(anotherFunc());

//Function Expression
/*
 * We can bind this on function declaration.
 * For this we have to use function expression.
 * this won't change as we bind the this value by 'bind'
 */
const profile = function (age, profession) {
    return `Hi ${this}. You are ${age} and you are a ${profession}.`;
}.bind('Saleh');

const outputCall3 = sayHi.call('Ahmad', 27, 'Web Developer');
console.log(outputCall);

const outputApply3 = sayHi.apply('Oyon', [27, 'Web Developer']);
console.log(outputApply);