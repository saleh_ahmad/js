const product = {
    name : 'T-Shirt',
    price : 25,
    'is available' : true,
    productDesc() {
        return `${this.name} - $${this.price}`;
    }
}

console.log(product.productDesc());