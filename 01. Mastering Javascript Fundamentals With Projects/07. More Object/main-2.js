//copy by value(primitive)

// let a = 10;
// const b = a;
// console.log(a) ;
// console.log(b) ;

// a = 20;
// console.log(a);
// console.log(b);

// //copy by reference(reference/complex);

// let obj1 = {value: 3};
// const obj2 = obj1;
// console.log(obj1)
// console.log(obj2)

// // obj1 = {value: 5}
// obj1.value = 5;
// console.log(obj1)
// console.log(obj2)

//pass by value
// let a = 10;
// function passByValue(a) {
//   a = true;
// }
// passByValue(a);
// console.log(a);
// //pass by reference
// let obj = {value: 10};
// function passByReference(passedObj) {
//   passedObj.value = 20;
// }
// passByReference(obj);
// console.log(obj);

// const product1 = {
//   name: 'T Shirt',
//   price: 20,
//   availability: true
// }
// let product2 = {};
// for(let key in product1){
//   product2[key] = product1[key]
// }
// console.log(product2)

// const product1 = {
//   name: 'T Shirt',
//   price: 20,
//   availability: true
// }
// const product2 = Object.assign({color: 'red'}, product1)
// console.log(product2)

// const product1 = {
//   name: 'T Shirt',
//   price: 20,
//   availability: true
// }
// const product2 = {...product1, color: 'red'}
// console.log(product2)

//primitive
//referenceType/complexType
//Object
//Function
//array

//Behind the scene Function is Object
// function sayHi(){
//   console.log('Hi')
// }
// console.dir(sayHi)//You can watch Function as object representation
// sayHi();
// //You can assign key-value to object but this part is not executable
// sayHi.firstName = 'samim';
// console.log(sayHi.firstName)

//Math object

//ceil(round to upper number)
//floor(round to lower number)
//random(generate a random number 0-1 but not including 1)
//abs(Generate a absolute number)
//round(Rounding a number either upper value or lower value)

//Generating a random number
console.log(Math.floor(Math.random() * 10));

//Generating a random number between min and max
// function getRandomInt(min, max) {
//   min = Math.ceil(min);
//   max = Math.floor(max);
//   return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
// }

// console.log(getRandomInt(2, 6));
// console.log(getRandomInt(2, 6));
// console.log(getRandomInt(2, 6));

// const now = new Date();
// let date = new Date(10000);
// date = new Date("December 17, 1995 03:24:00");
// date = new Date(2018, 10, 1, 10, 15, 50);
// date.setFullYear(2010);
// date.setMonth(1);
// console.log(date);
