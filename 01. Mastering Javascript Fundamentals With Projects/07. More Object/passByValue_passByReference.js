//Pass by value
let a = 10;

function passByValue(a) {
    a = 20;
}

passByValue(a);
console.log(a);

//pass by reference
let obj = {value : 10};

function passByReference(passedObj) {
    passedObj.value = 20;
}

passByReference(obj);
console.log(obj);