const numbers = [5, 3, 8, 7, 10];
const sortedArray = numbers.sort();

//Sort values according to ASCII values
console.log(sortedArray);

//Ascending Order
const ascSortedArr = numbers.sort((a, b) => ((a > b) ? 1 : -1) || 0);
console.log(ascSortedArr);

const descSortedArr = numbers.sort((a, b) => ((a < b) ? 1 : -1) || 0);
console.log(descSortedArr);

const products = [
    {
        id : 1,
        name : 'T-shirt',
        price : 10
    },
    {
        id : 3,
        name : 'Shoe',
        price : 60
    },
    {
        id : 2,
        name : 'Pant',
        price : 30
    }
];

//Sort By id
const sortedArr2 = products.sort((a, b) => ((a.id > b.id) ? 1 : -1) || 0);
console.log(sortedArr2);

//Sort By name
const sortedArr3 = products.sort((a, b) => {
    //return ((a.id > b.id) ? 1 : -1) || 0;
    const valueA = a.name.toLowerCase();
    const valueB = b.name.toLowerCase();

    return ((a.name > b.name) ? 1 : -1) || 0;
});
console.log(sortedArr3);