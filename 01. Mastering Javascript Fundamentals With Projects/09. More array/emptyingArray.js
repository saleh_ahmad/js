let numbers = [2, 4, 6, 8, 10];
numbers = [];
console.log(numbers);

let num = [2, 4, 6, 8, 10];
const num2 = num;

num = [];
console.log(num);
//Memory Leakage
console.log(num2);

const num3 = [2, 4, 6, 8, 10];
const num4 = num3;

num3.length = 0;

console.log(num3);
console.log(num4);
