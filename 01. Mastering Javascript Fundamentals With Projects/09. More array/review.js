const arr = [1, 3, 5, 7, 9];
console.log(arr);

//Getting Elements
console.log(arr[1]);

//Changing Elements
arr[2] = 6;
console.log(arr);

//Looping
for (let number of arr) {
    console.log(number);
}