const numbers = [2, 4, 6, 8, 10, 12, 14];

//Show -1 if element is not in the array
console.log(numbers.indexOf(3));

//Show index number if found the element
console.log(numbers.indexOf(2));
console.log(numbers.indexOf(8));

//Check the element is exist ot not
console.log(numbers.includes(4));
console.log(numbers.includes(2));
console.log(numbers.includes(3));
