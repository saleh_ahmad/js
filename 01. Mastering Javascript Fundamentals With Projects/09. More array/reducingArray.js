const numbers = [1, 4, 6, 10];
/*
 * acc= 1 cur= 4
 * acc= 5 cur= 6
 * acc= 11 cur= 10
 * acc= 21
 */
const sum = numbers.reduce((accumulator, current) => accumulator + current);

console.log(sum);

/*
 * acc = 10, current = 1
 * acc= 11 cur= 4
 * acc= 15 cur= 6
 * acc= 21 cur= 10
 * acc= 31
 */
const sum2 = numbers.reduce((accumulator, current) => (accumulator + current), 10);

console.log(sum2);