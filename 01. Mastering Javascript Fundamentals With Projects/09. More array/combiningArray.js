const num1 = [1, 3, 5, 7, 9],
    num2 = [2, 4, 6, 8, 10];

const result = num1.concat(num2);
console.log(result);

const combined = [num1, num2];
console.log(combined);

//Spread Operator
const combined2 = [...num1, ...num2];
console.log(combined2);