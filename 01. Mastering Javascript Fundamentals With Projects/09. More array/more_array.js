//const arr = [1, 3, 5, 8, 10];
// console.log(arr);
// //Getting elements
// console.log(arr[1])

// //changing elements
// arr[1] = 5;
// console.log(arr);

// //looping elements and making output
// for (let num of arr){
//   console.log(num)
// }

//Adding Elements
//===========================================
//const numbers = [1, 3, 5, 7, 10];
//Ending
// numbers.push(12);
// Beginning
// numbers.unshift(-3);

//Middle
//numbers.splice(2, 0, 4, 7, 9)

//Removing Elements
//========================================

//Ending
//numbers.pop(12);

// Beginning
//numbers.shift();

//Middle
//numbers.splice(2, 2)
//console.log(numbers)

//Finding elements in case of primitives
//=============================================
//const numbers = [1, 3, 5, 7, 10];

// const includedOrNot = numbers.indexOf('3');
// const result = (includedOrNot === -1)? 'Not included': 'included';
//const resultInclude = numbers.includes('3');
//console.log(resultInclude)

//Finding element of reference
//==============================================================
// const products = [
//   {
//     id: 1,
//     name: "T-Shirt",
//     price: 10
//   },
//   {
//     id: 3,
//     name: "T-Shirt",
//     price: 10
//   },
//   {
//     id: 2,
//     name: "shoes",
//     price: 60
//   }
// ];

// const result = products.find(function(productInfo) {
//   return productInfo.name === "T-Shirt";
// });
// const result = products.filter( productInfo =>  productInfo.name === "T-Shirt");
// console.log(result);

//combining Two array
//==================================================================

// const num1 = [2, 3, 4, 5];
// const num2 = [6, 7, 8, 9, 10];
//const result = num1.concat(num2);
//const combined = [...num1, ...num2];
//console.log(combined);

//Join is ued with array
// const result = num1.join(' '); //output string
//const str = 'I am samim';
//split is used with string
//const result = str.split(' ') //output array
//console.log(result);

//copying array
//======================================================

// The splice() method returns the removed item(s) in an array and slice() method returns the selected element(s) in an array, as a new array object.

// The splice() method changes the original array and slice() method doesn’t change the original array.

// The splice() method can take n number of arguments and slice() method takes 2 arguments.
//slice() method is more faster than splice() method

// const numbers = [1, 3, 5, 7, 10];
//const copiedArray = [...numbers];
//const copiedArray = numbers;
//const copiedArray = numbers.slice(0, numbers.length)
// const copiedArray = numbers.slice(0)

// console.log(copiedArray)

//Emptying array
//=================================================

// let numbers = [1, 3, 5, 7, 10];
//  const anotherNumbers = numbers;
// // numbers = [];
// numbers.length = 0;
// console.log(numbers)
// console.log(anotherNumbers)

//Iterating array
//=======================================================
//let numbers = [1, 3, 5, 7, 10];
// numbers.forEach(function(num,index, arr){
//   console.log(index);
//   //console.log(arr);
//   console.log(num)

// })

// for(let [index, number] of Object.entries(numbers)){
//   console.log(index,number)
// }

//sorting array
//==========================================================
// let numbers = [1, 7, 3, 9];

// const sortedArr = numbers.sort()

// console.log(sortedArr)

//you can also reverse the order
//const reversedArr = numbers.reverse();
//console.log(reversedArray)

// const products = [
//   {
//     id: 1,
//     name: "T-Shirt",
//     price: 10
//   },
//   {
//     id: 3,
//     name: "Microphne",
//     price: 10
//   },
//   {
//     id: 2,
//     name: "shoes",
//     price: 60
//   }
// ];
//const sortedResult = products.sort((a, b) => {
// a > b = 1
// a < b = -1
// a===b = 0
//solution 1
// const valueA = a.name.toLowerCase();
// const ValueB = b.name.toLowerCase();
// if(valueA > ValueB) return 1;
// if(valueA < ValueB) return -1;
// return 0;

//solution 2
// return ((a.id > b.id) ? 1 : -1) || 0;

//});
//console.log(sortedResult)

//Reduce function(used to make it small)
//========================================================
// const numbers = [1, 4, 6, 10];
//acc= 1 cur= 4
//acc= 5 cur= 6
//acc= 11 cur= 10
//acc= 21

//acc= 10 cur= 1
//acc= 11 cur= 4
//acc= 15 cur= 6
//acc= 21 cur= 10
//acc =31

// const sum = numbers.reduce((accumulator, current) => {
//   return accumulator + current;

// }, 10);
// console.log(sum);

//every and some method
//==========================================================
//return true or false
// let numbers = [1, 7, 3, 9];
// const result = numbers.every((number) => {
//   return number > 1;
// });
// const result = numbers.some(number => number > 1);
// console.log(result);

//map method
//=====================================================

//must return something
//result will be a array
// let numbers = [1, 7, 3, 9];

// const result = numbers
// .map(number => number * 2)
// .reduce((a, b) => a + b)
// ;
//This may not be appropriate
// const products = ['T-shirt', 'Shoes'];
// let ul = '<ul>'
// const result = products
// .map( item => ul +=` <li>${item}</li>`)
// .join(' ')
// ul += '</ul>'
// console.log(result);
// console.log(ul);

//Try another solution it might be better
// const products = ['T-shirt', 'Shoes'];
// let ul = '<ul>'
// const result = products
//   .map(item => ` <li>${item}</li>`)
//   .join(' ')
// ul += result + '</ul>'
// console.log(result);
// console.log(ul);
