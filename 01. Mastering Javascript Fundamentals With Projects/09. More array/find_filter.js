const products = [
    {
        id : 1,
        name : 'T-shirt',
        price : 10
    },
    {
        id : 2,
        name : 'T-shirt',
        price : 30
    },
    {
        id : 3,
        name : 'Shoe',
        price : 60
    }
];

/*
 * find
 * only show the first result
 */
const result = products.find(function(info) {
    return info.name === 'T-shirt'
});
console.log(result);

/*
 * filter
 * return array (all items)
 */
const result2 = products.filter(function(info) {
    return info.name === 'T-shirt'
});
console.log(result2);

/*
 * Arrow Function -> remove function keyword
 * 1 param -> Remove Bracket
 * 1 Line statement -> remove return keyword and curly braces
 */
const result3 = products.filter(info => info.name === 'T-shirt');

console.log(result3);