let numbers = [2, 4, 6, 8, 10];

//forEach
numbers.forEach(function(num, index, arr) {
    console.log(index);
    console.log(num);
});

//for of
for (let number of numbers) {
    console.log(number);
}

for (let number of Object.entries(numbers)) {
    console.log(number);
}

//Destructure
for (let [index, number] of Object.entries(numbers)) {
    console.log(index);
    console.log(number);
}