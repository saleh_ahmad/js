const numbers = [2, 4, 6, 8, 10, 12, 14];

//Ending
numbers.pop();
console.log(numbers);

//Starting
numbers.shift();
console.log(numbers);

//Middle
numbers.splice(2, 2);
console.log(numbers);