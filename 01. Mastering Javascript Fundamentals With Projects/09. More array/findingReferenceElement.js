const products = [
    {
        id : 1,
        name : 'T-shirt',
        price : 10
    },
    {
        id : 2,
        name : 'Shoe',
        price : 60
    }
];

/*
 * find
 * Callback function
 * Runs like a loop
 */
const result = products.find(function(info) {
    return info.name === 'T-shirt'
});

console.log(result);

const result2 = products.find(function(info) {
    return info.name === 'Shirt'
});

console.log(result2);

//Find Index
const result3 = products.findIndex(function(info) {
    return info.name === 'T-shirt'
});

console.log(result3);

const result4 = products.findIndex(function(info) {
    return info.name === 'Shirt'
});

console.log(result4);