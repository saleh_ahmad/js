//Exercise-1
/*
 *must result an array including all elements in the defined range
 * results will be [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
 */
function arrayFromRange(min, max) {
    const arr = [];
    do {
        arr.push(min);
        min++
    } while (min <= max);

    return arr;
}

const numbers = arrayFromRange(1, 10);

console.log(numbers);


//Exercise-2
/*
 * custom includes methods that is defined by you
 * result true or false based on searchElement
 */
//using includes
function includes(array, searchElement) {
    return array.includes(searchElement);
}

//using some
function includes2(array, searchElement) {
    return array.some(item => item === searchElement);
}

const numbers2 = [1, 2, 3, 4];
console.log(includes(numbers2, -1));
console.log(includes2(numbers2, 1));


//Exercise-3
/*
 * Exclude the numbers you passed to the function  from numbers array
 * result will be [3, 4]
 */
function except(array, excluded) {
    excluded.forEach((num) => {
        let cutIndex = array.indexOf(num);

        console.log(cutIndex);

        if (cutIndex >= 0) {
            array.splice(cutIndex, 1);
        }
    });

    return array;
}

const numbers3 = [1, 2, 3, 4];
const output = except(numbers3, [1, 2]);

console.log(output);


//Exercise-4
/*
 * count the occurrence of numbers
 * result will be number of occurrence .Example case: result will be 2
 * At first use procedural way
 * Then use reduce method
 */
function countOccurrences1(array, searchElement) {
    let count = 0;

    array.forEach(function(num) {
        count = num === searchElement ? count + 1 : count;
    });

    return count;
}

function countOccurrences2(array, searchElement) {
    let count = 0;

    array.reduce((accumulator, current) => {
        count = (current === searchElement) || (accumulator === searchElement)
            ? (count + 1)
            : count
    });

    return count;
}

const numbers4 = [1, 2, 3, 1];

const count = countOccurrences1(numbers4, 1);
console.log(count);
const count2 = countOccurrences2(numbers4, 1);
console.log(count2);


//Exercise-5
/*
 * Get the maximum number from the array
 * At first use procedural way
 * Then use reduce method
 */
function getMax1(array) {
    let max = 0;

    for (let el of array) {
        max = (el > max) ? el : max;
    }

    return max;
}

function getMax2(array) {
    let max = 0;

    array.reduce((a, b) => {
        if (a > max) {
            max = a;
        }

        if (b > max) {
            max = b
        }
    })

    return max;
}

const arr = [1, 2, 5, 4];
const max = getMax1(arr);
console.log(max);

const max2 = getMax2(arr);
console.log(max2);


