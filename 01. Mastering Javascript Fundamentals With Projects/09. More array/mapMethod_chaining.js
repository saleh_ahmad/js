/*
 * must return something
 * result will be array
 */

const numbers = [1, 7, 3, 9];

const result = numbers.map(number => number * 2);
console.log(result);

//chaining
const result2 = numbers
    .map(number => number * 2)
    .reduce((a, b) => a + b);

console.log(result2);

const product = ['T-Shirt', 'Shoes'];

let ul = '<ul>';
const result3 = product
    .map(item => ul += `<li>${item}</li>`)
    .join('');
ul += '</ul>';

console.log(ul);