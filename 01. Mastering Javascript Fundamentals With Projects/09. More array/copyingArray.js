const numbers = [2, 4, 6, 8, 10];

/*
 * Spread Operator
 * Best Way
 */
const copiedArray = [...numbers];
console.log(copiedArray);

numbers.push(12);
console.log(numbers);
console.log(copiedArray);

//Assign
const copiedArr2 = numbers;
console.log(copiedArr2);

numbers.push(14);
console.log(numbers);
console.log(copiedArr2);

//slice - Faster
//const combinedArr = numbers.slice(0, numbers.length);
const combinedArr = numbers.slice(0);
console.log(combinedArr);

//splice
//const combinedArr2 = numbers.splice(0, numbers.length);
const combinedArr2 = numbers.splice(0);
console.log(combinedArr2);
