const numbers = [1, 7, 3, 9];

//Every element must fulfill the condition
const resultEvery = numbers.every(number => number > 1);
console.log(resultEvery);

//Any element can fulfill the condition
const resultSome = numbers.some(number => number <= 1);
console.log(resultSome);