const numbers = [2, 4, 6, 8, 10];

//Ending
numbers.push(12);
console.log(numbers);

//Beginning
numbers.unshift(0);
console.log(numbers);

//Middle
numbers.splice(2, 0, 3, 5);
console.log(numbers);