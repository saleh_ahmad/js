console.log("Hello" && "World");
console.log("Hello" || "World");

//short circuiting and working with logical AND(&&) , OR(||) operator
console.log(true && true && false && true);
console.log(true || false || false || true);

//checking truth or falsy value
console.log(!!"Hello");
console.log(!!undefined);
console.log(!!"");

//operator precedence
console.log(1 + 2 * 5);
