// Exercise 1
const x = 10,
    y = 'a';

console.log(y === 'b' || x >= 10);
console.log('It will show true as use or operator and second part of the condition is true.');

// Exercise 2
const a = 3,
    b = 8;

console.log(!(a == '3' || a === b) && !(b != 8 && a <= b));
/*
 * !(true || false) && !(false && true)
 * !true && !false
 * false && true
 * false
 */

// Exercise 3
console.log(!'Hello World');
console.log(!'');
console.log(!0);
console.log(!-1);
console.log(!NaN);

// Exercise 4
const str = '',
    msg = 'haha!',
    isFunny = 'false';

console.log(!((str || msg) && isFunny));
/*
 * !((false || true) && true)
 * !(true && true)
 * !true
 * false
 */