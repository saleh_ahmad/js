console.log(!!'Hello');

// Falsy Value
console.log(!!undefined);
console.log(!!'');
console.log(!!NaN);
console.log(!!null);

console.log('Hello' && 'World');
console.log('Hello' || 'World');

console.log(true && true && false && true);
console.log(false || false || false || true);