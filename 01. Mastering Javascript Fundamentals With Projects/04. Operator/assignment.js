let x = 5;

x += 10;
x *= 10;
x--;
x++;
console.log(x);

// Post Increment
x++;

//Pre Increment
++x;

console.log(x++);
console.log(x);
console.log(++x);
console.log(x);